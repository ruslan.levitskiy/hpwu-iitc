// ==UserScript==
// @id           hpwu-iitc@lunarul
// @name         HPWU Tools
// @category     Layer
// @version      0.1.4-sync
// @description  Harry Potter: Wizards Unite tools over IITC
// @author       lunarul
// @match        https://www.ingress.com/intel*
// @match        https://ingress.com/intel*
// @match        https://intel.ingress.com/*
// @require 	 https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @downloadURL  https://gitlab.com/ruslan.levitskiy/hpwu-iitc/raw/master/hpwu.user.js
// @grant        none
// ==/UserScript==

/* eslint-env es6 */
/* eslint no-var: "error" */
/* globals L, S2, map */
/* globals GM_info, $, dialog */
/* globals renderPortalDetails, findPortalGuidByPositionE6 */

/** S2 Geometry functions

 S2 extracted from Regions Plugin
 https:static.iitc.me/build/release/plugins/regions.user.js

 the regional scoreboard is based on a level 6 S2 Cell
 - https:docs.google.com/presentation/d/1Hl4KapfAENAOf4gv-pSngKwvS_jwNVHRPZTTDzXXn6Q/view?pli=1#slide=id.i22
 at the time of writing there's no actual API for the intel map to retrieve scoreboard data,
 but it's still useful to plot the score cells on the intel map


 the S2 geometry is based on projecting the earth sphere onto a cube, with some scaling of face coordinates to
 keep things close to approximate equal area for adjacent cells
 to convert a lat,lng into a cell id:
 - convert lat,lng to x,y,z
 - convert x,y,z into face,u,v
 - u,v scaled to s,t with quadratic formula
 - s,t converted to integer i,j offsets
 - i,j converted to a position along a Hubbert space-filling curve
 - combine face,position to get the cell id

 NOTE: compared to the google S2 geometry library, we vary from their code in the following ways
 - cell IDs: they combine face and the hilbert curve position into a single 64 bit number. this gives efficient space
             and speed. javascript doesn't have appropriate data types, and speed is not cricical, so we use
             as [face,[bitpair,bitpair,...]] instead
 - i,j: they always use 30 bits, adjusting as needed. we use 0 to (1<<level)-1 instead
        (so GetSizeIJ for a cell is always 1)
*/

;function wrapperS2() {  // eslint-disable-line no-extra-semi

  const S2 = window.S2 = {};

  function LatLngToXYZ(latLng) {
    const d2r = Math.PI / 180.0;
    const phi = latLng.lat * d2r;
    const theta = latLng.lng * d2r;
    const cosphi = Math.cos(phi);

    return [Math.cos(theta) * cosphi, Math.sin(theta) * cosphi, Math.sin(phi)];
  }

  function XYZToLatLng(xyz) {
    const r2d = 180.0 / Math.PI;

    const lat = Math.atan2(xyz[2], Math.sqrt(xyz[0] * xyz[0] + xyz[1] * xyz[1]));
    const lng = Math.atan2(xyz[1], xyz[0]);

    return {lat: lat * r2d, lng: lng * r2d};
  }

  function largestAbsComponent(xyz) {
    const temp = [Math.abs(xyz[0]), Math.abs(xyz[1]), Math.abs(xyz[2])];

    if (temp[0] > temp[1]) {
      if (temp[0] > temp[2]) {
        return 0;
      }
      return 2;
    }

    if (temp[1] > temp[2]) {
      return 1;
    }

    return 2;
  }

  function faceXYZToUV(face,xyz) {
    let u, v;

    switch (face) {
      case 0: u =  xyz[1] / xyz[0]; v =  xyz[2] / xyz[0]; break;
      case 1: u = -xyz[0] / xyz[1]; v =  xyz[2] / xyz[1]; break;
      case 2: u = -xyz[0] / xyz[2]; v = -xyz[1] / xyz[2]; break;
      case 3: u =  xyz[2] / xyz[0]; v =  xyz[1] / xyz[0]; break;
      case 4: u =  xyz[2] / xyz[1]; v = -xyz[0] / xyz[1]; break;
      case 5: u = -xyz[1] / xyz[2]; v = -xyz[0] / xyz[2]; break;
      default: throw {error: 'Invalid face'};
    }

    return [u,v];
  }

  function XYZToFaceUV(xyz) {
    let face = largestAbsComponent(xyz);

    if (xyz[face] < 0) {
      face += 3;
    }

    const uv = faceXYZToUV(face, xyz);

    return [face, uv];
  }

  function FaceUVToXYZ(face, uv) {
    const u = uv[0];
    const v = uv[1];

    switch (face) {
      case 0: return [1, u, v];
      case 1: return [-u, 1, v];
      case 2: return [-u,-v, 1];
      case 3: return [-1,-v,-u];
      case 4: return [v,-1,-u];
      case 5: return [v, u,-1];
      default: throw {error: 'Invalid face'};
    }
  }

  function STToUV(st) {
    const singleSTtoUV = function (st) {
      if (st >= 0.5) {
        return (1 / 3.0) * (4 * st * st - 1);
      }
      return (1 / 3.0) * (1 - (4 * (1 - st) * (1 - st)));

    };

    return [singleSTtoUV(st[0]), singleSTtoUV(st[1])];
  }

  function UVToST(uv) {
    const singleUVtoST = function (uv) {
      if (uv >= 0) {
        return 0.5 * Math.sqrt (1 + 3 * uv);
      }
      return 1 - 0.5 * Math.sqrt (1 - 3 * uv);

    };

    return [singleUVtoST(uv[0]), singleUVtoST(uv[1])];
  }

  function STToIJ(st,order) {
    const maxSize = 1 << order;

    const singleSTtoIJ = function (st) {
      const ij = Math.floor(st * maxSize);
      return Math.max(0, Math.min(maxSize - 1, ij));
    };

    return [singleSTtoIJ(st[0]), singleSTtoIJ(st[1])];
  }

  function IJToST(ij,order,offsets) {
    const maxSize = 1 << order;

    return [
      (ij[0] + offsets[0]) / maxSize,
      (ij[1] + offsets[1]) / maxSize
    ];
  }

  // S2Cell class
  S2.S2Cell = function () {};

  //static method to construct
  S2.S2Cell.FromLatLng = function (latLng, level) {
    const xyz = LatLngToXYZ(latLng);
    const faceuv = XYZToFaceUV(xyz);
    const st = UVToST(faceuv[1]);
    const ij = STToIJ(st,level);

    return S2.S2Cell.FromFaceIJ(faceuv[0], ij, level);
  };

  S2.S2Cell.FromFaceIJ = function (face, ij, level) {
    const cell = new S2.S2Cell();
    cell.face = face;
    cell.ij = ij;
    cell.level = level;

    return cell;
  };

  S2.S2Cell.prototype.toString = function () {
    return 'F' + this.face + 'ij[' + this.ij[0] + ',' + this.ij[1] + ']@' + this.level;
  };

  S2.S2Cell.prototype.getLatLng = function () {
    const st = IJToST(this.ij, this.level, [0.5, 0.5]);
    const uv = STToUV(st);
    const xyz = FaceUVToXYZ(this.face, uv);

    return XYZToLatLng(xyz);
  };

  S2.S2Cell.prototype.getCornerLatLngs = function () {
    const offsets = [
      [0.0, 0.0],
      [0.0, 1.0],
      [1.0, 1.0],
      [1.0, 0.0]
    ];

    return offsets.map(offset => {
      const st = IJToST(this.ij, this.level, offset);
      const uv = STToUV(st);
      const xyz = FaceUVToXYZ(this.face, uv);

      return XYZToLatLng(xyz);
    });
  };

  S2.S2Cell.prototype.getNeighbors = function (deltas) {

    const fromFaceIJWrap = function (face,ij,level) {
      const maxSize = 1 << level;
      if (ij[0] >= 0 && ij[1] >= 0 && ij[0] < maxSize && ij[1] < maxSize) {
        // no wrapping out of bounds
        return S2.S2Cell.FromFaceIJ(face,ij,level);
      }

      // the new i,j are out of range.
      // with the assumption that they're only a little past the borders we can just take the points as
      // just beyond the cube face, project to XYZ, then re-create FaceUV from the XYZ vector
      let st = IJToST(ij,level,[0.5, 0.5]);
      let uv = STToUV(st);
      let xyz = FaceUVToXYZ(face, uv);
      const faceuv = XYZToFaceUV(xyz);
      face = faceuv[0];
      uv = faceuv[1];
      st = UVToST(uv);
      ij = STToIJ(st,level);
      return S2.S2Cell.FromFaceIJ(face, ij, level);
    };

    const face = this.face;
    const i = this.ij[0];
    const j = this.ij[1];
    const level = this.level;

    if (!deltas) {
      deltas = [
        {a: -1, b: 0},
        {a: 0, b: -1},
        {a: 1, b: 0},
        {a: 0, b: 1}
      ];
    }
    return deltas.map(function (values) {
      return fromFaceIJWrap(face, [i + values.a, j + values.b], level);
    });
  };
}

/** Our code
* For safety, S2 must be initialized before our code
*
* Code is modified from the Pokemon GO plugin
* https://gitlab.com/AlfonsoML/pogo-s2/raw/master/s2check.user.js
*/
function wrapperPlugin(plugin_info) {
  'use strict';

  // based on https://github.com/iatkin/leaflet-svgicon
  function initSvgIcon() {
    L.DivIcon.SVGIcon = L.DivIcon.extend({
      options: {
        'className': 'svg-icon',
        'iconAnchor': null, //defaults to [iconSize.x/2, iconSize.y] (point tip)
        'iconSize': L.point(48, 48)
      },
      initialize: function (options) {
        options = L.Util.setOptions(this, options);

        //iconSize needs to be converted to a Point object if it is not passed as one
        options.iconSize = L.point(options.iconSize);

        if (!options.iconAnchor) {
          options.iconAnchor = L.point(Number(options.iconSize.x) / 2, Number(options.iconSize.y));
        } else {
          options.iconAnchor = L.point(options.iconAnchor);
        }
      },

      // https://github.com/tonekk/Leaflet-Extended-Div-Icon/blob/master/extended.divicon.js#L13
      createIcon: function (oldIcon) {
        let div = L.DivIcon.prototype.createIcon.call(this, oldIcon);

        if (this.options.id) {
          div.id = this.options.id;
        }

        if (this.options.style) {
          for (let key in this.options.style) {
            div.style[key] = this.options.style[key];
          }
        }
        return div;
      }
    });

    L.divIcon.svgIcon = function (options) {
      return new L.DivIcon.SVGIcon(options);
    };

    L.Marker.SVGMarker = L.Marker.extend({
      options: {
        'iconFactory': L.divIcon.svgIcon,
        'iconOptions': {}
      },
      initialize: function (latlng, options) {
        options = L.Util.setOptions(this, options);
        options.icon = options.iconFactory(options.iconOptions);
        this._latlng = latlng;
      },
      onAdd: function (map) {
        L.Marker.prototype.onAdd.call(this, map);
      }
    });

    L.marker.svgMarker = function (latlng, options) {
      return new L.Marker.SVGMarker(latlng, options);
    };
  }

  /**
   * Saves a file to disk with the provided text
   * @param {string} text - The text to save
   * @param {string} filename - Proposed filename
   */
  function saveToFile(text, filename) {
    if (typeof text != 'string') {
      text = JSON.stringify(text);
    }

    if (typeof window.android !== 'undefined' && window.android.saveFile) {
      window.android.saveFile(filename, 'application/json', text);
      return;
    }

    if (isIITCm()) {
      promptForCopy(text);
      return;
    }

    // http://stackoverflow.com/a/18197341/250294
    const element = document.createElement('a');
    // fails with large amounts of data
    // element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));

    // http://stackoverflow.com/questions/13405129/javascript-create-and-save-file
    const file = new Blob([text], {type: 'text/plain'});
    element.setAttribute('href', URL.createObjectURL(file));

    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  /**
   * Prompts the user to select a file and then reads its contents and calls the callback function with those contents
   * @param {Function} callback - Function that will be called when the file is read.
   * Callback signature: function( {string} contents ) {}
   */
  function readFromFile(callback) {
    // special hook from iitcm
    if (typeof window.requestFile != 'undefined') {
      window.requestFile(function (filename, content) {
        callback(content);
      });
      return;
    }

    if (isIITCm()) {
      promptForPaste(callback);
      return;
    }

    const input = document.createElement('input');
    input.type = 'file';
    input.className = 'baseutils-filepicker';
    document.body.appendChild(input);

    input.addEventListener('change', function () {
      const reader = new FileReader();
      reader.onload = function () {
        callback(reader.result);
      };
      reader.readAsText(input.files[0]);
      document.body.removeChild(input);
    }, false);

    input.click();
  }

  function promptForPaste(callback) {
    const div = document.createElement('div');

    const textarea = document.createElement('textarea');
    textarea.style.width = '100%';
    textarea.style.minHeight = '8em';
    div.appendChild(textarea);

    const container = dialog({
      id: 'promptForPaste',
      html: div,
      width: '360px',
      title: 'Paste here the data',
      buttons: {
        OK: function () {
          container.dialog('close');
          callback(textarea.value);
        }
      }
    });
  }

  function promptForCopy(text) {
    const div = document.createElement('div');

    const textarea = document.createElement('textarea');
    textarea.style.width = '100%';
    textarea.style.minHeight = '8em';
    textarea.value = text;
    div.appendChild(textarea);

    const container = dialog({
      id: 'promptForCopy',
      html: div,
      width: '360px',
      title: 'Copy this data',
      buttons: {
        OK: function () {
          container.dialog('close');
        }
      }
    });
  }

  const TIMERS = {};
  function createThrottledTimer(name, callback, ms) {
    if (TIMERS[name])
      clearTimeout(TIMERS[name]);

    // throttle if there are several calls to the functions
    TIMERS[name] = setTimeout(function() {
      delete TIMERS[name];
      if (typeof window.requestIdleCallback == 'undefined')
        callback();
      else
        // and even now, wait for iddle
        requestIdleCallback(function() {
          callback();
        }, { timeout: 2000 });

    }, ms || 100);
  }

  /**
   * Try to identify if the browser is IITCm due to special bugs like file picker not working
   */
  function isIITCm() {
    const ua = navigator.userAgent;
    if (!ua.match(/Android.*Mobile/))
      return false;

    if (ua.match(/; wb\)/))
      return true;

    return ua.match(/ Version\//);
  }

  let inns = {};
  let fortresses = {};
  let greenhouses = {};
  // Portals that aren't marked as HPWU items
  let nothpwu = {};

  let allPortals = {};
  let newPortals = {};
  let checkNewPortalsTimout;

  // Portals that the user hasn't classified (2 or more in the same Lvl17 cell)
  let skippedPortals = {};
  // let newPokestops = {};
  let notClassifiedPois = [];

  // Portals that we know, but that have been moved from our stored location.
  let movedPortals = [];

  // HPWU items that are no longer available.
  let missingPortals = {};

  // Cells currently detected with extra gyms
  //let cellsExtraGyms = {};
  // Cells that the user has marked to ignore extra gyms
  //let ignoredCellsExtraGyms = {};
  // Cells with missing Gyms
  //let ignoredCellsMissingGyms = {};

  // Leaflet layers
  let regionLayer; // s2 grid
  let innLayerGroup; // inns
  let fortressLayerGroup; // fortresses
  let greenhouseLayerGroup; // greenhouses
  let nearbyGroupLayer; // circles to mark the too near limit

  // Group of items added to the layer
  let innLayers = {};
  let fortressLayers = {};
  let greenhouseLayers = {};
  let nearbyCircles = {};

  const defaultSettings = {
    //highlightGymCandidateCells: false,
    //highlightGymCenter: false,
    thisIsHPWU: false,
    analyzeForMissingData: true,
    grids: [
      {
        level: 14,
        width: 5,
        color: '#004D40',
        opacity: 0.5
      },
      {
        level: 0,
        width: 2,
        color: '#388E3C',
        opacity: 0.5
      }
    ],
    colors: {
      // cellsExtraGyms: {
      //   color: '#ff0000',
      //   opacity: 0.5
      // },
      // cellsMissingGyms: {
      //   color: '#ffa500',
      //   opacity: 0.5
      // },
      cell17Filled: {
        color: '#000000',
        opacity: 0.6
      },
      cell14Filled: {
        color: '#000000',
        opacity: 0.5
      },
      nearbyCircleBorder: {
        color: '#000000',
        opacity: 0.6
      },
      nearbyCircleFill: {
        color: '#000000',
        opacity: 0.4
      },
      // missingStops1: {
      //   color: '#BF360C',
      //   opacity: 1
      // },
      // missingStops2: {
      //   color: '#E64A19',
      //   opacity: 1
      // },
      // missingStops3: {
      //   color: '#FF5722',
      //   opacity: 1
      // }
    },
    saveDataType: 'Inns',
    saveDataFormat: 'CSV'
  };

  let settings = defaultSettings;

  function saveSettings() {
    createThrottledTimer('saveSettings', function() {
      localStorage['s2check_settings'] = JSON.stringify(settings);
    });
  }

  function loadSettings() {
    const tmp = localStorage['s2check_settings'];
    if (!tmp)
      return;
    try  {
      settings = JSON.parse(tmp);
    } catch (e) { // eslint-disable-line no-empty
    }
    if (typeof settings.analyzeForMissingData == 'undefined') {
      settings.analyzeForMissingData = true;
    }
    if (typeof settings.promptForMissingData != 'undefined') {
      delete settings.promptForMissingData;
    }
    if (!settings.colors) {
      resetColors();
    }
    if (typeof settings.saveDataType == 'undefined') {
      settings.saveDataType = 'Inns';
    }
    if (typeof settings.saveDataFormat == 'undefined') {
      settings.saveDataFormat = 'CSV';
    }

    setThisIsHPWU();
  }

  function resetColors() {
    settings.grids[0].color = defaultSettings.grids[0].color;
    settings.grids[0].opacity = defaultSettings.grids[0].opacity;
    settings.grids[1].color = defaultSettings.grids[1].color;
    settings.grids[1].opacity = defaultSettings.grids[1].opacity;
    settings.colors = defaultSettings.colors;
  }

  let originalHighlightPortal;

  function setThisIsHPWU() {
    document.body.classList[settings.thisIsHPWU ? 'add' : 'remove']('thisIsHPWU');

    if (settings.thisIsHPWU) {
      removeIngressLayers();
      if (window._current_highlighter == window._no_highlighter) {
        // extracted from IITC plugin: Hide portal ownership

        originalHighlightPortal = window.highlightPortal;
        window.highlightPortal = portal => {
          window.portalMarkerScale();
          const hidePortalOwnershipStyles = window.getMarkerStyleOptions({team: window.TEAM_NONE, level: 0});
          portal.setStyle(hidePortalOwnershipStyles);
        };
        window.resetHighlightedPortals();
      }
    } else {
      restoreIngressLayers();
      if (originalHighlightPortal != null) {
        window.highlightPortal = originalHighlightPortal;
        originalHighlightPortal = null;
        window.resetHighlightedPortals();
      }
    }
  }

  function sortByName(a, b) {
    if (!a.name)
      return -1;

    return a.name.localeCompare(b.name);
  }

  function isCellOnScreen(mapBounds, cell) {
    const corners = cell.getCornerLatLngs();
    const cellBounds = L.latLngBounds([corners[0],corners[1]]).extend(corners[2]).extend(corners[3]);
    return cellBounds.intersects(mapBounds);
  }

  // return only the cells that are visible by the map bounds to ignore far away data that might not be complete
  function filterWithinScreen(cells) {
    const bounds = map.getBounds();
    const filtered = {};
    Object.keys(cells).forEach(cellId => {
      const cellData = cells[cellId];
      const cell = cellData.cell;

      if (isCellInsideScreen(bounds, cell)) {
        filtered[cellId] = cellData;
      }
    });
    return filtered;
  }

  function isCellInsideScreen(mapBounds, cell) {
    const corners = cell.getCornerLatLngs();
    const cellBounds = L.latLngBounds([corners[0],corners[1]]).extend(corners[2]).extend(corners[3]);
    return mapBounds.contains(cellBounds);
  }

  /**
  * Filter a group of items (inns/fortresses/greenhouses) excluding those out of the screen
  */
  function filterItemsByMapBounds(items) {
    const bounds = map.getBounds();
    const filtered = {};
    Object.keys(items).forEach(id => {
      const item = items[id];

      if (isPointOnScreen(bounds, item)) {
        filtered[id] = item;
      }
    });
    return filtered;
  }

  function isPointOnScreen(mapBounds, point) {
    if (point._latlng)
      return mapBounds.contains(point._latlng);

    return mapBounds.contains(L.latLng(point));
  }

  function groupByCell(level) {
    const cells = {};
    classifyGroup(cells, inns, level, (cell, item) => cell.inns.push(item));
    classifyGroup(cells, fortresses, level, (cell, item) => cell.fortresses.push(item));
    classifyGroup(cells, greenhouses, level, (cell, item) => cell.greenhouses.push(item));
    classifyGroup(cells, newPortals, level, (cell, item) => cell.notClassified.push(item));
    classifyGroup(cells, nothpwu, level, (cell, item) => {/* */});

    return cells;
  }

  function classifyGroup(cells, items, level, callback) {
    Object.keys(items).forEach(id => {
      const item = items[id];
      if (!item.cells) {
        item.cells = {};
      }
      let cell;
      // Compute the cell only once for each level
      if (!item.cells[level]) {
        cell = window.S2.S2Cell.FromLatLng(item, level);
        item.cells[level] = cell.toString();
      }
      const cellId = item.cells[level];

      // Add it to the array of POIs of that cell
      if (!cells[cellId]) {
        if (!cell) {
          cell = window.S2.S2Cell.FromLatLng(item, level);
        }
        cells[cellId] = {
          cell: cell,
          inns: [],
          fortresses: [],
          greenhouses: [],
          notClassified: []
        };
      }
      callback(cells[cellId], item);
    });
  }

  /**
   * Returns the items that belong to the specified cell
   */
  function findCellItems(cellId, level, items) {
    return Object.values(items).filter(item => {
      return item.cells[level] == cellId;
    });
  }

  /**
    Tries to add the portal photo when exporting from Ingress.com/intel
  */
  function findPhotos(items) {
    if (!window.portals) {
      return items;
    }
    Object.keys(items).forEach(id => {
      const item = items[id];
      if (item.image)
        return;

      const portal = window.portals[id];
      if (portal && portal.options && portal.options.data) {
        item.image = portal.options.data.image;
      }
    });
    return items;
  }

  function configureGridLevelSelect(select, i) {
    select.value = settings.grids[i].level;
    select.addEventListener('change', e => {
      settings.grids[i].level = parseInt(select.value, 10);
      saveSettings();
      updateMapGrid();
    });
  }

  function showS2Dialog() {
    const selectRow = `
      <p>{{level}} level of grid to display: <select>
      <option value=0>None</option>
      <option value=6>6</option>
      <option value=7>7</option>
      <option value=8>8</option>
      <option value=9>9</option>
      <option value=10>10</option>
      <option value=11>11</option>
      <option value=12>12</option>
      <option value=13>13</option>
      <option value=14>14</option>
      <option value=15>15</option>
      <option value=16>16</option>
      <option value=17>17</option>
      <option value=18>18</option>
      <option value=19>19</option>
      <option value=20>20</option>
      </select></p>`;

    const html =
      selectRow.replace('{{level}}', '1st') +
      selectRow.replace('{{level}}', '2nd') +
      `<!-- p><label><input type="checkbox" id="chkHighlightCandidates">Highlight Cells that might get a Gym</label></p>
      <p><label><input type="checkbox" id="chkHighlightCenters">Highlight centers of Cells with a Gym</label></p -->
      <p><label title='Hide Ingress panes, info and whatever that clutters the map and it is useless for HPWU'><input type="checkbox" id="chkThisIsHPWU">This is HPWU!</label></p>
      <p><label title="Analyze the portal data to show the pane that suggests new POIs"><input type="checkbox" id="chkanalyzeForMissingData">Analyze portal data</label></p>
      <p><a id='HPWUEditColors'>Colors</a></p>
       `;

    const container = dialog({
      id: 's2Settings',
      width: 'auto',
      html: html,
      title: 'S2 & HPWU Settings'
    });

    const div = container[0];

    const selects = div.querySelectorAll('select');
    for (let i = 0; i < 2; i++) {
      configureGridLevelSelect(selects[i], i);
    }

    // const chkHighlight = div.querySelector('#chkHighlightCandidates');
    // chkHighlight.checked = settings.highlightGymCandidateCells;
    //
    // chkHighlight.addEventListener('change', e => {
    //   settings.highlightGymCandidateCells = chkHighlight.checked;
    //   saveSettings();
    //   updateMapGrid();
    // });
    //
    // const chkHighlightCenters = div.querySelector('#chkHighlightCenters');
    // chkHighlightCenters.checked = settings.highlightGymCenter;
    // chkHighlightCenters.addEventListener('change', e => {
    //   settings.highlightGymCenter = chkHighlightCenters.checked;
    //   saveSettings();
    //   updateMapGrid();
    // });

    const chkThisIsHPWU = div.querySelector('#chkThisIsHPWU');
    chkThisIsHPWU.checked = !!settings.thisIsHPWU;
    chkThisIsHPWU.addEventListener('change', e => {
      settings.thisIsHPWU = chkThisIsHPWU.checked;
      saveSettings();
      setThisIsHPWU();
    });

    const chkanalyzeForMissingData = div.querySelector('#chkanalyzeForMissingData');
    chkanalyzeForMissingData.checked = !!settings.analyzeForMissingData;
    chkanalyzeForMissingData.addEventListener('change', e => {
      settings.analyzeForMissingData = chkanalyzeForMissingData.checked;
      saveSettings();
      if (newPortals.length > 0) {
        checkNewPortals();
      }
    });

    const HPWUEditColors = div.querySelector('#HPWUEditColors');
    HPWUEditColors.addEventListener('click', function (e) {
      editColors();
      e.preventDefault();
      return false;
    });
  }

  function editColors() {
    const selectRow = `<p class='hpwu-colors'>{{title}}<br>
      Color: <input type='color' id='{{id}}Color'> Opacity: <select id='{{id}}Opacity'>
      <option value=0>0</option>
      <option value=0.1>0.1</option>
      <option value=0.2>0.2</option>
      <option value=0.3>0.3</option>
      <option value=0.4>0.4</option>
      <option value=0.5>0.5</option>
      <option value=0.6>0.6</option>
      <option value=0.7>0.7</option>
      <option value=0.8>0.8</option>
      <option value=0.9>0.9</option>
      <option value=1>1</option>
      </select></p>`;

    const html =
      selectRow.replace('{{title}}', '1st Grid').replace(/{{id}}/g, 'grid0') +
      selectRow.replace('{{title}}', '2nd Grid').replace(/{{id}}/g, 'grid1') +
      // selectRow.replace('{{title}}', 'Cells with extra gyms').replace(/{{id}}/g, 'cellsExtraGyms') +
      // selectRow.replace('{{title}}', 'Cells with missing gyms').replace(/{{id}}/g, 'cellsMissingGyms') +
      // selectRow.replace('{{title}}', 'Cell 17 with a gym or stop').replace(/{{id}}/g, 'cell17Filled') +
      // selectRow.replace('{{title}}', 'Cell 14 with 3 gyms').replace(/{{id}}/g, 'cell14Filled') +
      selectRow.replace('{{title}}', 'Border of too close circles').replace(/{{id}}/g, 'nearbyCircleBorder') +
      selectRow.replace('{{title}}', 'Fill of too close circles').replace(/{{id}}/g, 'nearbyCircleFill') +
      // selectRow.replace('{{title}}', '1 more stop to get a gym').replace(/{{id}}/g, 'missingStops1') +
      // selectRow.replace('{{title}}', '2 more stops to get a gym').replace(/{{id}}/g, 'missingStops2') +
      // selectRow.replace('{{title}}', '3 more stops to get a gym').replace(/{{id}}/g, 'missingStops3') +
      '<a id="resetColorsLink">Reset all colors</a>'
      ;

    const container = dialog({
      id: 's2Colors',
      width: 'auto',
      html: html,
      title: 'HPWU grid Colors'
    });

    const div = container[0];

    const updatedSetting = function (id) {
      saveSettings();
      if (id == 'nearbyCircleBorder' || id == 'nearbyCircleFill') {
        redrawNearbyCircles();
      } else {
        updateMapGrid();
      }
    };

    const configureItems = function (key, item, id) {
      if (!id)
        id = item;

      const entry = settings[key][item];
      const select = div.querySelector('#' + id + 'Opacity');
      select.value = entry.opacity;
      select.addEventListener('change', function (event) {
        settings[key][item].opacity = select.value;
        updatedSetting(id);
      });

      const input = div.querySelector('#' + id + 'Color');
      input.value = entry.color;
      input.addEventListener('change', function (event) {
        settings[key][item].color = input.value;
        updatedSetting(id);
      });
    };

    configureItems('grids', 0, 'grid0');
    configureItems('grids', 1, 'grid1');
    // configureItems('colors', 'cellsExtraGyms');
    // configureItems('colors', 'cellsMissingGyms');
    // configureItems('colors', 'cell17Filled');
    // configureItems('colors', 'cell14Filled');
    configureItems('colors', 'nearbyCircleBorder');
    configureItems('colors', 'nearbyCircleFill');
    // configureItems('colors', 'missingStops1');
    // configureItems('colors', 'missingStops2');
    // configureItems('colors', 'missingStops3');

    const resetColorsLink = div.querySelector('#resetColorsLink');
    resetColorsLink.addEventListener('click', function() {
      container.dialog('close');
      resetColors();
      updatedSetting('nearbyCircleBorder');
      updatedSetting();
      editColors();
    });
  }

  /**
   * Refresh the S2 grid over the map
   */
  function updateMapGrid() {
    updatePois();
    regionLayer.clearLayers();

    if (!map.hasLayer(regionLayer))
      return;

    const bounds = map.getBounds();
    const seenCells = {};
    const drawCellAndNeighbors = function (cell, color, width, opacity) {
      const cellStr = cell.toString();

      if (!seenCells[cellStr]) {
        // cell not visited - flag it as visited now
        seenCells[cellStr] = true;

        if (isCellOnScreen(bounds, cell)) {
          // on screen - draw it
          drawCell(cell, color, width, opacity);

          // and recurse to our neighbors
          const neighbors = cell.getNeighbors();
          for (let i = 0; i < neighbors.length; i++) {
            drawCellAndNeighbors(neighbors[i], color, width, opacity);
          }
        }
      }
    };

    // center cell
    const zoom = map.getZoom();
    if (zoom < 5) {
      return;
    }
    // first draw nearby circles at the bottom
    if (16 < zoom) {
      regionLayer.addLayer(nearbyGroupLayer);
    }
    // then draw the cell grid
    for (let i = 0; i < settings.grids.length; i++) {
      const grid = settings.grids[i];
      const gridLevel = grid.level;
      if (gridLevel >= 6 && gridLevel < (zoom + 2)) {
        const cell = S2.S2Cell.FromLatLng(getLatLngPoint(map.getCenter()), gridLevel);
        drawCellAndNeighbors(cell, grid.color, grid.width, grid.opacity);
      }
    }
    // if (settings.highlightGymCandidateCells && 12 < zoom) {
    //   updateCandidateCells();
    // }
    // if (settings.highlightGymCenter && 16 < zoom) {
    //   updateGymCenters();
    // }
  }

  /**
   * Refresh the pois storage from the cloud
   */
  function updatePois() {
    const bounds = map.getBounds();
    console.log(bounds);
    $.ajax({
      type: "POST",
      url: "https://mapplugin.hpwutgn.ru/object/bounds",
      data: JSON.stringify(bounds),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (data) {
        $.each(data.response, function (k, v) {
          switch (v.type) {
            case "inns":
              v.synced = true;
              inns[v.guid] = v;
              break;
            case "fortresses":
              v.synced = true;
              fortresses[v.guid] = v;
              break;
            case "greenhouses":
              v.synced = true;
              greenhouses[v.guid] = v;
              break;
            case "nothpwu":
              v.synced = true;
              nothpwu[v.guid] = v;
              break;
          }
        });
        thisPlugin.saveStorage();
        thisPlugin.resetAllMarkers();
      },
      error: function (errMsg) {
      }
    });

  }


  function getLatLngPoint(data) {
    const result = {
      lat: typeof data.lat == 'function' ? data.lat() : data.lat,
      lng: typeof data.lng == 'function' ? data.lng() : data.lng
    };

    return result;
  }

  /**
   * Highlight cells that are missing a few stops to get another gym
   * based on data from https://www.reddit.com/r/TheSilphRoad/comments/7ppb3z/gyms_pok%C3%A9stops_and_s2_cells_followup_research/
   * Cut offs: 2, 6, 20
   */
  // function updateCandidateCells() {
  //   const level = 14;
  //   // All cells with items
  //   const allCells = groupByCell(level);
  //
  //   const bounds = map.getBounds();
  //   const seenCells = {};
  //   const cellsToDraw = {
  //     1: [],
  //     2: [],
  //     3: []
  //   };
  //
  //   const drawCellAndNeighbors = function (cell) {
  //     const cellStr = cell.toString();
  //
  //     if (!seenCells[cellStr]) {
  //       // cell not visited - flag it as visited now
  //       seenCells[cellStr] = true;
  //
  //       if (isCellOnScreen(bounds, cell)) {
  //         // on screen - draw it
  //         const cellData = allCells[cellStr];
  //         if (cellData) {
  //           const missingGyms = computeMissingGyms(cellData);
  //           if (missingGyms > 0 && !ignoredCellsMissingGyms[cellStr]) {
  //             fillCell(cell, settings.colors.cellsMissingGyms.color, settings.colors.cellsMissingGyms.opacity);
  //           } else if (missingGyms < 0 && !ignoredCellsExtraGyms[cellStr]) {
  //             fillCell(cell, settings.colors.cellsExtraGyms.color, settings.colors.cellsExtraGyms.opacity);
  //             if (!cellsExtraGyms[cellStr]) {
  //               cellsExtraGyms[cellStr] = true;
  //               updateCounter('extraGyms', Object.keys(cellsExtraGyms));
  //             }
  //           }
  //           const missingStops = computeMissingStops(cellData);
  //           switch (missingStops) {
  //             case 0:
  //               coverBlockedAreas(cellData);
  //               if (missingGyms == 0) {
  //                 fillCell(cell, settings.colors.cell14Filled.color, settings.colors.cell14Filled.opacity);
  //               }
  //               break;
  //             case 1:
  //             case 2:
  //             case 3:
  //               cellsToDraw[missingStops].push(cell);
  //               coverBlockedAreas(cellData);
  //               writeInCell(cell, missingStops);
  //               break;
  //             default:
  //               coverBlockedAreas(cellData);
  //               writeInCell(cell, missingStops);
  //               break;
  //           }
  //         }
  //
  //         // and recurse to our neighbors
  //         const neighbors = cell.getNeighbors();
  //         for (let i = 0; i < neighbors.length; i++) {
  //           drawCellAndNeighbors(neighbors[i]);
  //         }
  //       }
  //     }
  //   };
  //
  //   const cell = S2.S2Cell.FromLatLng(getLatLngPoint(map.getCenter()), level);
  //   drawCellAndNeighbors(cell);
  //   // Draw missing cells in reverse order
  //   for (let missingStops = 3; missingStops >= 1; missingStops--) {
  //     const color = settings.colors['missingStops' + missingStops].color;
  //     const opacity = settings.colors['missingStops' + missingStops].opacity;
  //     cellsToDraw[missingStops].forEach(cell => drawCell(cell, color, 3, opacity));
  //   }
  // }

  /**
   * Draw a cross to the center of level 20 cells that have a Gym to check better EX locations
   */
  // function updateGymCenters() {
  //   const visibleGyms = filterItemsByMapBounds(gyms);
  //   const level = 20;
  //
  //   Object.keys(visibleGyms).forEach(id => {
  //     const gym = gyms[id];
  //     const cell = window.S2.S2Cell.FromLatLng(gym, level);
  //     const corners = cell.getCornerLatLngs();
  //     // center point
  //     const center = cell.getLatLng();
  //
  //     const style = {fill: false, color: 'red', opacity: 0.8, weight: 1, clickable: false, interactive: false};
  //     const line1 = L.polyline([corners[0], corners[2]], style);
  //     regionLayer.addLayer(line1);
  //
  //     const line2 = L.polyline([corners[1], corners[3]], style);
  //     regionLayer.addLayer(line2);
  //
  //     const circle = L.circle(center, 1, style);
  //     regionLayer.addLayer(circle);
  //   });
  // }
  //
  // function coverBlockedAreas(cellData) {
  //   if (!cellData)
  //     return;
  //   cellData.gyms.forEach(coverLevel17Cell);
  //   cellData.stops.forEach(coverLevel17Cell);
  // }
  //
  // function coverLevel17Cell(point) {
  //   const cell = S2.S2Cell.FromLatLng(point, 17);
  //   fillCell(cell, settings.colors.cell17Filled.color, settings.colors.cell17Filled.opacity);
  // }
  //
  // // Computes how many new stops must be added to the L14 Cell to get a new Gym
  // function computeMissingStops(cellData) {
  //   const sum = cellData.gyms.length + cellData.stops.length;
  //   if (sum < 2)
  //     return 2 - sum;
  //
  //   if (sum < 6)
  //     return 6 - sum;
  //
  //   if (sum < 20)
  //     return 20 - sum;
  //
  //   // No options to more gyms ATM.
  //   return 0;
  // }

  // Checks if the L14 cell has enough Gyms and Stops and one of the stops should be marked as a Gym
  // If the result is negative then it has extra gyms
  // function computeMissingGyms(cellData) {
  //   const totalGyms = cellData.gyms.length;
  //   const sum = totalGyms + cellData.stops.length;
  //   if (sum < 2)
  //     return 0 - totalGyms;
  //
  //   if (sum < 6)
  //     return 1 - totalGyms;
  //
  //   if (sum < 20)
  //     return 2 - totalGyms;
  //
  //   return 3 - totalGyms;
  // }

  function drawCell(cell, color, weight, opacity) {
    // corner points
    const corners = cell.getCornerLatLngs();

    // the level 6 cells have noticible errors with non-geodesic lines - and the larger level 4 cells are worse
    // NOTE: we only draw two of the edges. as we draw all cells on screen, the other two edges will either be drawn
    // from the other cell, or be off screen so we don't care
    const region = L.polyline([corners[0], corners[1], corners[2], corners[3], corners[0]], {fill: false, color: color, opacity: opacity, weight: weight, clickable: false, interactive: false});

    regionLayer.addLayer(region);
  }

  function fillCell(cell, color, opacity) {
    // corner points
    const corners = cell.getCornerLatLngs();

    const region = L.polygon(corners, {color: color, fillOpacity: opacity, weight: 0, clickable: false, interactive: false});
    regionLayer.addLayer(region);
  }

  /**
  *  Writes a text in the center of a cell
  */
  function writeInCell(cell, text) {
    // center point
    let center = cell.getLatLng();

    let marker = L.marker(center, {
      icon: L.divIcon({
        className: 's2check-text',
        iconAnchor: [25, 5],
        iconSize: [50, 10],
        html: text
      }),
      interactive: false
    });
    // fixme, maybe add some click handler

    regionLayer.addLayer(marker);
  }

  // ***************************
  // IITC code
  // ***************************


  // ensure plugin framework is there, even if iitc is not yet loaded
  if (typeof window.plugin !== 'function') {
    window.plugin = function () {};
  }

  // PLUGIN START ////////////////////////////////////////////////////////

  // use own namespace for plugin
  window.plugin.hpwu = function () {};

  const thisPlugin = window.plugin.hpwu;
  const KEY_STORAGE = 'plugin-hpwu';

  /*********************************************************************************************************************/

  // Update the localStorage
  thisPlugin.saveStorage = function () {
    createThrottledTimer('saveStorage', function() {
      localStorage[KEY_STORAGE] = JSON.stringify({
        inns: cleanUpExtraData(inns),
        fortresses: cleanUpExtraData(fortresses),
        greenhouses: cleanUpExtraData(greenhouses),
        nothpwu: cleanUpExtraData(nothpwu),
        // ignoredCellsExtraGyms: ignoredCellsExtraGyms,
        // ignoredCellsMissingGyms: ignoredCellsMissingGyms
      });
    });
  };

  thisPlugin.postObject = function (obj) {
    let request = {
      "agent": window.PLAYER.nickname,
      "faction": window.PLAYER.team,
      "level": window.PLAYER.level,
      "object": obj
    };
    $.ajax({
      type: "POST",
      url: "https://mapplugin.hpwutgn.ru/object",
      data: JSON.stringify(request),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (data) {
      },
      error: function (errMsg) {
      }
    });
  };


  /**
   * Create a new object where the extra properties of each POI have been removed. Store only the minimum.
   */
  function cleanUpExtraData(group) {
    let newGroup = {};
    Object.keys(group).forEach(id => {
      const data = group[id];
      const newData = {
        guid: data.guid,
        lat: data.lat,
        lng: data.lng,
        name: data.name,
        synced: !!data.synced
      };

      if (data.sponsored)
        newData.sponsored = data.sponsored;

      if (data.color)
        newData.color = data.color;

      newGroup[id] = newData;
    });
    return newGroup;
  }

  // Load the localStorage
  thisPlugin.loadStorage = function () {
    const tmp = JSON.parse(localStorage[KEY_STORAGE] || '{}');
    inns = tmp.inns || {};
    fortresses = tmp.fortresses || {};
    greenhouses = tmp.greenhouses || {};
    nothpwu = tmp.nothpwu || {};
    // ignoredCellsExtraGyms = tmp.ignoredCellsExtraGyms || {};
    // ignoredCellsMissingGyms = tmp.ignoredCellsMissingGyms || {};
  };

  // Load the localStorage
  thisPlugin.syncLS = function () {
    $.each(inns, function (k, v) {
      if (!v.synced) {
        v.type = "inns";
        thisPlugin.postObject(v);
        inns[v.guid].synced = true;
      }
    });
    $.each(fortresses, function (k, v) {
      if (!v.synced) {
        v.type = "fortresses";
        thisPlugin.postObject(v);
        fortresses[v.guid].synced = true;
      }
    });
    $.each(greenhouses, function (k, v) {
      if (!v.synced) {
        v.type = "greenhouses";
        thisPlugin.postObject(v);
        greenhouses[v.guid].synced = true;
      }
    });
    $.each(nothpwu, function (k, v) {
      if (!v.synced) {
        v.type = "nothpwu";
        thisPlugin.postObject(v);
        nothpwu[v.guid].synced = true;
      }
    });
    thisPlugin.saveStorage();
  };

  thisPlugin.createEmptyStorage = function () {
    inns = {};
    fortresses = {};
    greenhouses = {};
    nothpwu = {};
    // ignoredCellsExtraGyms = {};
    // ignoredCellsMissingGyms = {};
    thisPlugin.saveStorage();

    allPortals = {};
    newPortals = {};

    movedPortals = [];
    missingPortals = {};
  };

  /*************************************************************************/

  thisPlugin.findByGuid = function (guid) {
    if (inns[guid]) {
      return {'type': 'inns', 'store': inns};
    }
    if (fortresses[guid]) {
      return {'type': 'fortresses', 'store': fortresses};
    }
    if (greenhouses[guid]) {
      return {'type': 'greenhouses', 'store': greenhouses};
    }
    if (nothpwu[guid]) {
      return {'type': 'nothpwu', 'store': nothpwu};
    }
    return null;
  };

  // Append a 'star' flag in sidebar.
  thisPlugin.onPortalSelectedPending = false;
  thisPlugin.onPortalSelected = function () {
    $('.hpwuInn').remove();
    $('.hpwuFortress').remove();
    $('.hpwuGreenhouse').remove();
    $('.notHPWU').remove();
    const portalDetails = document.getElementById('portaldetails');
    portalDetails.classList.remove('isInn');

    if (window.selectedPortal == null) {
      return;
    }

    if (!thisPlugin.onPortalSelectedPending) {
      thisPlugin.onPortalSelectedPending = true;

      setTimeout(function () { // the sidebar is constructed after firing the hook
        thisPlugin.onPortalSelectedPending = false;

        $('.hpwuInn').remove();
        $('.hpwuFortress').remove();
        $('.hpwuGreenhouse').remove();
        $('.notHPWU').remove();

        // Show HPWU icons in the mobile status-bar
        if (thisPlugin.isSmart) {
          document.querySelector('.HPWUStatus').innerHTML = thisPlugin.htmlStar;
          $('.HPWUStatus > a').attr('title', '');
        }

        $(portalDetails).append('<div class="HPWUButtons">Wizards Unite: ' + thisPlugin.htmlStar + '</div>' +
          `<div id="HPWUInnInfo">
          <label for='HPWUInnColor'>Color:</label> <select id='HPWUInnColor'>
              <option value='Green'>Green</option>
              <option value='Pink'>Pink</option>
              <option value='Blue'>Blue</option>
              <option value='Brown'>Brown</option>
              <option value='Purple'>Purple</option>
              <option value='White'>White (sponsored)</option>
              </select><br>
        </div>`);

        document.getElementById('HPWUInnColor').addEventListener('change', ev => {
          const guid = window.selectedPortal;
          const icon = document.getElementById('inn' + guid.replace('.', ''));
          // remove styling of inn marker
          if (icon) {
            icon.classList.remove(inns[guid].color + 'Color');
          }
          inns[guid].color = ev.target.value;
          let objToPost = inns[guid];
          objToPost.type = "inns";
          thisPlugin.postObject(objToPost);
          inns[guid].synced = true;
          thisPlugin.saveStorage();
          // update inn marker
          if (icon) {
            icon.classList.add(inns[guid].color + 'Color');
          }
        });

        // document.getElementById('PogoGymEx').addEventListener('change', ev => {
        //   const guid = window.selectedPortal;
        //   const icon = document.getElementById('gym' + guid.replace('.', ''));
        //   gyms[guid].isEx = ev.target.checked;
        //   thisPlugin.saveStorage();
        //   // update gym marker
        //   if (icon) {
        //     icon.classList[gyms[guid].isEx ? 'add' : 'remove']('exGym');
        //   }
        // });

        thisPlugin.updateStarPortal();
      }, 0);
    }
  };

  // Update the status of the star (when a portal is selected from the map/hpwu-list)
  thisPlugin.updateStarPortal = function () {
    $('.hpwuInn').removeClass('favorite');
    $('.hpwuFortress').removeClass('favorite');
    $('.hpwuGreenhouse').removeClass('favorite');
    $('.notHPWU').removeClass('favorite');
    document.getElementById('portaldetails').classList.remove('isInn');

    const guid = window.selectedPortal;
    // If current portal is in hpwu: select hpwu portal from portals list and select the star
    const hpwuData = thisPlugin.findByGuid(guid);
    if (hpwuData) {
      if (hpwuData.type === 'fortresses') {
        $('.hpwuFortress').addClass('favorite');
      }
      if (hpwuData.type === 'inns') {
        $('.hpwuInn').addClass('favorite');
        document.getElementById('portaldetails').classList.add('isInn');
        const inn = inns[guid];
        if (inn.color) {
          document.getElementById('HPWUInnColor').value = inn.color;
        }
        // document.getElementById('PogoGymEx').checked = gym.isEx;

      }
      if (hpwuData.type === 'greenhouses') {
        $('.hpwuGreenhouse').addClass('favorite');
      }
      if (hpwuData.type === 'nothpwu') {
        $('.notHPWU').addClass('favorite');
      }
    }
  };

  function removeHPWUObject(type, guid) {
    if (type === 'fortresses') {
      delete fortresses[guid];
      const starInLayer = fortressLayers[guid];
      fortressLayerGroup.removeLayer(starInLayer);
      delete fortressLayers[guid];
    }
    if (type === 'inns') {
      delete inns[guid];
      const innInLayer = innLayers[guid];
      innLayerGroup.removeLayer(innInLayer);
      delete innLayers[guid];
    }
    if (type === 'greenhouses') {
      delete greenhouses[guid];
      const greenhouseInLayer = greenhouseLayers[guid];
      greenhouseLayerGroup.removeLayer(greenhouseInLayer);
      delete greenhouseLayers[guid];
    }
    if (type === 'nothpwu') {
      delete nothpwu[guid];
    }
  }

  // Switch the status of the star
  thisPlugin.switchStarPortal = function (type) {
    const guid = window.selectedPortal;

    // It has been manually classified, remove from the detection
    if (newPortals[guid])
      delete newPortals[guid];

    // If portal is saved in HPWU: Remove this POI
    const hpwuData = thisPlugin.findByGuid(guid);
    if (hpwuData) {
      const existingType = hpwuData.type;
      removeHPWUObject(existingType, guid);

      thisPlugin.saveStorage();
      thisPlugin.updateStarPortal();

      // Get portal name and coordinates
      const p = window.portals[guid];
      const ll = p.getLatLng();
      if (existingType !== type) {
        thisPlugin.addPortalHPWU(guid, ll.lat, ll.lng, p.options.data.title, type);
      }
      // we've changed one item from pogo, if the cell was marked as ignored, reset it.
      // if (updateExtraGymsCells(ll.lat, ll.lng))
      //   thisPlugin.saveStorage();
    } else {
      // If portal isn't saved in HPWU: Add this POI

      // Get portal name and coordinates
      const portal = window.portals[guid];
      const latlng = portal.getLatLng();
      thisPlugin.addPortalHPWU(guid, latlng.lat, latlng.lng, portal.options.data.title, type);
    }

    // if (settings.highlightGymCandidateCells) {
    //   updateMapGrid();
    // }
  };

  // Add portal
  thisPlugin.addPortalHPWU = function (guid, lat, lng, name, type, color) {
    // Add POI in the localStorage
    const obj = {'guid': guid, 'lat': lat, 'lng': lng, 'name': name};

    // prevent that it would trigger the missing portal detection if it's in our data
    if (window.portals[guid]) {
      obj.exists = true;
    }

    if (type == 'inns') {
      obj.color = color || 'Green';
      inns[guid] = obj;
    }
    if (type == 'fortresses') {
      fortresses[guid] = obj;
    }
    if (type == 'greenhouses') {
      greenhouses[guid] = obj;
    }
    if (type == 'nothpwu') {
      nothpwu[guid] = obj;
    }

    let objToPost = obj;
    objToPost.type = type;
    thisPlugin.postObject(objToPost);
    obj.synced = true;
    // updateExtraGymsCells(lat, lng);
    thisPlugin.saveStorage();
    thisPlugin.updateStarPortal();

    thisPlugin.addStar(guid, lat, lng, name, type, obj.color);
  };

  /**
   * An item has been changed in a cell, check if the cell should no longer be ignored
   */
  // function updateExtraGymsCells(lat, lng) {
  //   if (Object.keys(ignoredCellsExtraGyms).length == 0 && Object.keys(ignoredCellsMissingGyms).length == 0)
  //     return false;
  //
  //   const cell = window.S2.S2Cell.FromLatLng(new L.LatLng(lat, lng), 14);
  //   const cellId = cell.toString();
  //   if (ignoredCellsExtraGyms[cellId]) {
  //     delete ignoredCellsExtraGyms[cellId];
  //     return true;
  //   }
  //   if (ignoredCellsMissingGyms[cellId]) {
  //     delete ignoredCellsMissingGyms[cellId];
  //     return true;
  //   }
  //   return false;
  // }

  /*
    OPTIONS
  */
  // Manual import, export and reset data
  thisPlugin.hpwuActionsDialog = function () {
    const content = `<div id="hpwuSetbox">
      <a id="save-dialog" title="Select the data to save from the info on screen">Save...</a>
      <a onclick="window.plugin.hpwu.optReset();return false;" title="Deletes all HPWU markers">Reset HPWU portals</a>
      <a onclick="window.plugin.hpwu.optImport();return false;" title="Import a JSON file with all the HPWU data">Import HPWU</a>
      <a onclick="window.plugin.hpwu.optExport();return false;" title="Exports a JSON file with all the HPWU data">Export HPWU</a>
      </div>`;

    const container = dialog({
      html: content,
      title: 'S2 & HPWU Actions'
    });

    const div = container[0];
    div.querySelector('#save-dialog').addEventListener('click', e => saveDialog());
  };

  function saveDialog() {
    const content = `<div>
      <p>Select the data to save from the info on screen</p>
      <fieldset><legend>Which data?</legend>
      <input type='radio' name='HPWUSaveDataType' value='Inns' id='HPWUSaveDataTypeInns'><label for='HPWUSaveDataTypeInns'>Inns</label><br>
      <input type='radio' name='HPWUSaveDataType' value='Fortresses' id='HPWUSaveDataTypeFortresses'><label for='HPWUSaveDataTypeFortresses'>Fortresses</label><br>
      <input type='radio' name='HPWUSaveDataType' value='Greenhouses' id='HPWUSaveDataTypeGreenhouses'><label for='HPWUSaveDataTypeGreenhouses'>Greenhouses</label><br>
      <input type='radio' name='HPWUSaveDataType' value='All' id='HPWUSaveDataTypeAll'><label for='HPWUSaveDataTypeAll'>All</label>
      </fieldset>
      <fieldset><legend>Format</legend>
      <input type='radio' name='HPWUSaveDataFormat' value='CSV' id='HPWUSaveDataFormatCSV'><label for='HPWUSaveDataFormatCSV'>CSV</label><br>
      <input type='radio' name='HPWUSaveDataFormat' value='JSON' id='HPWUSaveDataFormatJSON'><label for='HPWUSaveDataFormatJSON'>JSON</label>
      </fieldset>
      <fieldset><legend>Wizards Unite Map</legend>
      <input type='checkbox' name='HPWUSaveForWUW' value='1' id='HPWUSaveForWUW'><label for='HPWUSaveForWUW'>Format for WUW Map</label>
      </fieldset>
      </div>`;

    function escapeCSV(s) {
      if (s === 0) {
        return '0';
      }
      if (s === undefined || s === null) {
        return '';
      }
      if (/[,"\n]/.test(s)) {
        return '"' + s.replace(/"/g, '""') + '"';
      }

      return s;
    }

    const wuwColors = {
      Green: 'Green-roofed',
      Blue: 'Blue-roofed',
      Brown: 'Brown-roofed',
      Purple: 'Purple-roofed',
      Pink: 'Pink-walled',
      White: 'White-walled'
    };

    function mapToCSV(arr, type, wuw = false) {
        const data = filterItemsByMapBounds(arr);
        const keys = Object.keys(data);
        return keys.map(id => {
            const poi = data[id];
            return [poi.name, poi.lat, poi.lng, type, wuw ? wuwColors[poi.color] : poi.color].map(escapeCSV).join(',');
        });
    }

    const container = dialog({
      html: content,
      title: 'Save visible data',
      buttons: {
        'Save': function () {
          const SaveDataType = document.querySelector('input[name="HPWUSaveDataType"]:checked').value;
          const SaveDataFormat = document.querySelector('input[name="HPWUSaveDataFormat"]:checked').value;
          const SaveForWUW = !!document.querySelector('input[name="HPWUSaveForWUW"]').checked;
          const types = ['Inns', 'Fortresses', 'Greenhouses', 'All'];
          if (types.indexOf(SaveDataType) < 0) {
            SaveDataType = 'All';
          }

          settings.saveDataType = SaveDataType;
          settings.saveDataFormat = SaveDataFormat;
          settings.saveForWUW = SaveForWUW;
          saveSettings();

          container.dialog('close');

          let filename = SaveDataType.toLowerCase() + '_' + (new Date()).toISOString().substr(0, 19).replace(/[\D]/g, '_');
          if (SaveDataFormat == 'CSV') {
            filename += '.csv';
            let rows = [];
            if (SaveForWUW) {
                rows.push('"Name","Latitude","Longitude","Type","Inn Colour"');
            }
            if (SaveDataType == 'All' || SaveDataType == 'Inns') {
                rows = [...rows, ...mapToCSV(inns, 'inn', SaveForWUW)];
            }
            if (SaveDataType == 'All' || SaveDataType == 'Fortresses') {
                rows = [...rows, ...mapToCSV(fortresses, 'fortress', SaveForWUW)];
            }
            if (SaveDataType == 'All' || SaveDataType == 'Greenhouses') {
                rows = [...rows, ...mapToCSV(greenhouses, 'greenhouse', SaveForWUW)];
            }

            saveToFile(rows.join('\n'), filename);
          } else {
            filename += '.json';
            let data = {};
            if (SaveDataType == 'All' || SaveDataType == 'Inns') {
              data.inns = filterItemsByMapBounds(inns);
            };
            if (SaveDataType == 'All' || SaveDataType == 'Fortresses') {
              data.fortresses = filterItemsByMapBounds(fortresses);
            };
            if (SaveDataType == 'All' || SaveDataType == 'Greenhouses') {
              data.greenhouses = filterItemsByMapBounds(greenhouses);
            };
            if (SaveForWUW) {
              let mapped = [];
              Object.keys(data).forEach(key => {
                mapped = [...mapped, ...Object.values(data[key]).map(poi => ({
                  name: poi.name,
                  latitude: poi.lat,
                  longitude: poi.lng,
                  type: key.slice(0, -1),
                  colour: wuwColors[poi.color]
                }))];
              });
              data = mapped;
            } else {
              Object.keys(data).forEach(key => {
                data[key] = findPhotos(cleanUpExtraData(data[key]));
              });
            }

            saveToFile(JSON.stringify(data), filename);
          }
        }
      }

    });

    // Remove ok button
    const outer = container.parent();
    outer.find('.ui-dialog-buttonset button:first').remove();

    const div = container[0];
    div.querySelector('#HPWUSaveDataType' + settings.saveDataType).checked = true;
    div.querySelector('#HPWUSaveDataFormat' + settings.saveDataFormat).checked = true;
    div.querySelector('#HPWUSaveForWUW').checked = !!settings.saveForWUW;

  };

  thisPlugin.optAlert = function (message) {
    $('.ui-dialog .ui-dialog-buttonset').prepend('<p class="hpwu-alert" style="float:left;margin-top:4px;">' + message + '</p>');
    $('.hpwu-alert').delay(2500).fadeOut();
  };

  thisPlugin.optExport = function () {
    saveToFile(localStorage[KEY_STORAGE], 'IITC-hpwu.json');
  };

  thisPlugin.optImport = function () {
    readFromFile(function (content) {
      try {
        const list = JSON.parse(content); // try to parse JSON first
        let importExStatus = true;
        let importInnColor = true;
        Object.keys(list).forEach(type => {
          for (let idhpwu in list[type]) {
            const item = list[type][idhpwu];
            const lat = item.lat;
            const lng = item.lng;
            const name = item.name;
            let guid = item.guid;
            if (!guid) {
              guid = findPortalGuidByPositionE6(lat * 1E6, lng * 1E6);
              if (!guid) {
                console.log('portal guid not found', name, lat, lng); // eslint-disable-line no-console
                guid = idhpwu;
              }
            }

            if (typeof lat !== "undefined" && typeof lng !== "undefined" && name && !thisPlugin.findByGuid(guid)) {
              thisPlugin.addPortalHPWU(guid, lat, lng, name, type, item.color);
            }
          }
        });

        thisPlugin.updateStarPortal();
        thisPlugin.resetAllMarkers();
        thisPlugin.optAlert('Successful.');
      } catch (e) {
        console.warn('HPWU: failed to import data: ' + e); // eslint-disable-line no-console
        thisPlugin.optAlert('<span style="color: #f88">Import failed </span>');
      }
    });
  };

  thisPlugin.optReset = function () {
    if (confirm('All HPWU data will be deleted. Are you sure?', '')) {
      delete localStorage[KEY_STORAGE];
      thisPlugin.createEmptyStorage();
      thisPlugin.updateStarPortal();
      thisPlugin.resetAllMarkers();
      // if (settings.highlightGymCandidateCells) {
      //   updateMapGrid();
      // }
      thisPlugin.optAlert('Successful.');
    }
  };

  /* HPWU PORTALS LAYER */
  thisPlugin.addAllMarkers = function () {
    function iterateStore(store, type) {
      for (let idhpwu in store) {
        const item = store[idhpwu];
        thisPlugin.addStar(item.guid, item.lat, item.lng, item.name, type, item.color);
      }
    }

    iterateStore(inns, 'inns');
    iterateStore(fortresses, 'fortresses');
    iterateStore(greenhouses, 'greenhouses');
  };

  thisPlugin.resetAllMarkers = function () {
    for (let guid in fortressLayers) {
      const starInLayer = fortressLayers[guid];
      fortressLayerGroup.removeLayer(starInLayer);
      delete fortressLayers[guid];
    }
    for (let innGuid in innLayers) {
      const innInLayer = innLayers[innGuid];
      innLayerGroup.removeLayer(innInLayer);
      delete innLayers[innGuid];
    }
    for (let greenhouseGuid in greenhouseLayers) {
      const greenhouseInLayer = greenhouseLayers[greenhouseGuid];
      greenhouseLayerGroup.removeLayer(greenhouseInLayer);
      delete greenhouseLayers[greenhouseGuid];
    }
    thisPlugin.addAllMarkers();
  };

  thisPlugin.addStar = function (guid, lat, lng, name, type, color) {
    let star;
    if (type === 'fortresses') {
      star = new L.Marker.SVGMarker([lat, lng], {
        title: name,
        iconOptions: {
          className: 'fortress',
          html: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 163.3 410.6"><path class="fortress-base" d="M113.8 351.9l6.6 13.9 1.7 18.8-11 12-60.8 2.5-12.4-3.5-12.3-14.8 2.8-22.7 5.4-2 37-4.2z"/><path class="fortress-steps" d="M38.9 358c-.1 0-2.1 16.2 20.9 18.1 23 1.9 44.7-10.3 44.7-10.3l-33.7-13.9-31.9 6.1z"/><path class="fortress-steps" d="M46.6 360.8s-.3 8.6 13.9 10.3c14.2 1.7 33.6-5.3 33.6-5.3"/><path class="fortress-flag" d="M83.1 37.6l.4-25 21.1 5.7-21.3 4.3"/><path class="fortress-wall" d="M119.4 185.3l-8 85.7-68.3 2.6-4.4-86z"/><path class="fortress-roof" d="M83.1 37.6l44 145 6.3 10s-24.3 5.1-54.3 4.7C50 197 27.4 192 27.4 192l17.3-28.7L83.1 37.6z"/><path class="fortress-window" d="M61.7 156.3l6.5-7.5 8.5 8.5v19.4H62.4z"/><g><path class="fortress-wall fortress-thin" d="M110.8 129.6l-3.3 20s4.8 21.3 14.5 26l9.2-46h-20.4z"/><path class="fortress-roof fortress-thin" d="M130.3 74.3l-25 55.3h31.3z"/><path class="fortress-wall fortress-thin" d="M148.1 120.6l-7.9 40.2h-15.4l8.5-41.6z"/><path class="fortress-roof fortress-thin" d="M150.1 77.6l-22 41.2 24.2 1.8z"/></g><g><path class="fortress-flag" d="M25.1 138.1l-2.7-23 18.7 2.4-17.7 6.1"/><path class="fortress-wall" d="M57.3 228.6l5.8 130.5-16.5 1.7-12.8-4.7-17.2-127.5 14.7 2.2z"/><path class="fortress-roof" d="M16.6 228.6l-6.8-3 6.8-13.3 8.5-74.2 25.5 72 14.2 12-7.5 6.5-26 2.2z"/><path class="fortress-window" d="M23.6 219.2l8.4 11.5v13.4l-8-.8-2.7-14z"/></g><g><path class="fortress-wall" d="M58.8 361.6l21 2 8.3-4.3 4-95.2-14.5 4.7-27.3-4.7z"/><path class="fortress-door" d="M64.8 362.1s-.6-22.2 5.7-21.7c7.1.6 6.2 22.8 6.2 22.8l-11.9-1.1z"/><path class="fortress-roof" d="M47.5 263.6l20.6-53.8 26.3 53.5-16.8 5.5z"/><path class="fortress-window" d="M59.1 265.4l5.9-6.3 5.9 8.5-1.3 15.1H59.1z"/></g><g><path class="fortress-flag" d="M122.1 172.1l2.8-21.6 21 7.8-22.1.3"/><path class="fortress-wall" d="M111.6 365.8l17.5-108-23.8 2.5-15.2-2.5-5 103 9 5z"/><path class="fortress-roof" d="M90.1 257.8l15.2 2.5 23.8-2.5 7.5-6.5-6.3-11.7-8.2-67.5-28.3 68.5-8.7 11.2z"/><path class="fortress-window" d="M111.1 259.7l9.1-10.3 3.4 9-3.8 15.9h-8.7z"/></g></svg>`,
          iconSize: L.point(24, 62),
          iconAnchor: [11, 54]
        }
      });

    }
    if (type === 'inns') {
      const className = 'inn ' + color + 'Color';
      star = new L.Marker.SVGMarker([lat, lng], {
        title: name,
        iconOptions: {
          id: 'inn' + guid.replace('.', ''),
          className: className,
          html: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 310 425.3"><path class="inn-wall" d="M63.3 395l-13.2-99.2L169.8 94l73.3 116.5 4 102.3-4 89.7-25 15.8-127-5.3z"/><path class="inn-door" d="M153.7 413v-64s2-17.3 27.7-16.3 27.3 19 27.3 19l1.7 63.3-56.7-2z"/><path class="inn-window" d="M88.8 338l37.5-2.5 4.4 59.7-39.4-2z"/><path class="inn-roof" d="M121 76.7L8 53l15 135L8 293.3 53.3 311l67 2 38.7-92.3 14-95L211.3 227l24.4 87.7 65.6-10.6L281 187.3 285 50l-71.3 22.3z"/><g class="inn-tower"><path class="inn-roof" d="M91 7.3L57.3 54.8l2 50.7 58.1 11 7.9-59.8z"/><path class="inn-window" d="M75.1 65.1h28L100.3 93l-23.7-1.7z"/></g></svg>`,
          iconSize: L.point(24, 32),
          iconAnchor: [12, 26]
        }
      });
    }
    if (type === 'greenhouses') {
      star = new L.Marker.SVGMarker([lat, lng], {
        title: name,
        iconOptions: {
          className: 'greenhouse',
          html: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 321.5 354.5"><path class="greenhouse-bottom" d="M28.3 273.5l8.7 57 67 3.5 32.3 10.5h50.6l40.9-16 60.5 1.3 4.7-54-69.5-1-34 11.2-55.3-1-39.4-13z"/><path class="greenhouse-panes" d="M8.8 164.5h68L75.5 141l12.8-59.5 58-35.7 13.2-31.5 13.3 30.5 58.7 37 13 59.2-1.7 23.5 69-1.5-5 59-13 52.3h-70.3l-33.5 9.2h-55.7l-39.5-11-66.8 1.3L13.5 223z"/><path class="greenhouse-frame greenhouse-thick" d="M75.5 141l39.5 15.3 94.3-.5 34.9-16.1"/><path class="greenhouse-frame" d="M88.3 81.5l39.2 7h67.8l36.2-6.7M11 192.2l73.5 1.1 28.8 12.7h91.5l31.2-12.5 73.1.5M13.5 223l77.8 7 23.7 8.3 87.8 4.5 25.7-9.9 78.3-10.9"/><path class="greenhouse-frame" d="M76.8 164.5l7.7 28.8 6.8 36.7 3.5 42 9.2 62M115.5 278.8l-.5-40.5-1.7-32.3 1.7-49.7 12.5-67.8 28-39.7 4-34.5L165 48l30.3 40.5 14 67.3-4.5 50.2-2 36.8 4.9 37.2M227.8 328.5l-4.3-53.7 5-41.9 7.5-39.4 6.8-29"/><path class="greenhouse-panes greenhouse-thick" d="M136.3 344.5s-13-148 26-149.3S187 344.5 187 344.5h-50.7z"/><g><path class="greenhouse-frame" d="M162.3 195.3l1.2 149.2M136.4 246.7h51.3M134.3 283.5H190M134.6 315.3h54.2"/></g></svg>`,
          iconSize: L.point(29, 32),
          iconAnchor: [15, 25]
        }
      });

    }

    if (!star)
      return;

    window.registerMarkerForOMS(star);
    star.on('spiderfiedclick', function () {
      // don't try to render fake portals
      if (guid.indexOf('.') > -1) {
        renderPortalDetails(guid);
      }
    });

    if (type === 'fortresses') {
      fortressLayers[guid] = star;
      star.addTo(fortressLayerGroup);
    }
    if (type === 'inns') {
      innLayers[guid] = star;
      star.addTo(innLayerGroup);
    }
    if (type === 'greenhouses') {
      greenhouseLayers[guid] = star;
      star.addTo(greenhouseLayerGroup);
    }
  };

  thisPlugin.setupCSS = function () {
    $('<style>').prop('type', 'text/css').html(`
#sidebar #portaldetails h3.title{
  width:auto;
}
.hpwuFortress span,
.hpwuGreenhouse span,
.hpwuInn span {
  display:inline-block;
  float:left;
  margin:3px 1px 0 4px;
  width:24px;
  height:24px;
  overflow:hidden;
  background-repeat:no-repeat;
  background-size:contain;
}
.hpwuFortress span,
.hpwuGreenhouse span,
.hpwuInn span {
  filter:grayscale(100%);
}
.hpwuFortress:focus span, .hpwuFortress.favorite span,
.hpwuGreenhouse:focus span, .hpwuGreenhouse.favorite span,
.hpwuInn:focus span, .hpwuInn.favorite span {
  filter:none;
}

/**********************************************
  DIALOG BOX
**********************************************/

/*---- Options panel -----*/
#hpwuSetbox a{
  display:block;
  color:#ffce00;
  border:1px solid #ffce00;
  padding:3px 0;
  margin:10px auto;
  width:80%;
  text-align:center;
  background:rgba(8,48,78,.9);
}
#hpwuSetbox a.disabled,
#hpwuSetbox a.disabled:hover{
  color:#666;
  border-color:#666;
  text-decoration:none;
}

#hpwuSetbox{
  text-align:center;
}
.hpwuFortress span {
  background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAABoVBMVEUAAADBu67BvK8Ae8Xm4tHc18etp5jl4NDBvK2nmo7h3MxoUknf3Mu1r6DAvK/l4dDRzb7Bu6/LxLXo4dLWzsDm4tPj3s8wKzq/v6/o4taxpJgAgNP/ABPRxroAgc/d2cm5s6PWz8Hd2cnMxrfxAAoAidsAdrzc1ca5s6QAkecAcbTm4dBPo9RGns5GqeBClsQ+jrk4ha7BvK65s6Po49O2saHl4NG5s6Pp5NS3sKGeDhfX0cPMx7bl39C5s6TV0MCyrJ4EecAEdLfl4NC6s6S3r6Pn5NPwAAmsppe4sqUAl/UAb67l4c8Ai9oAdLy4sKHIw7S3sqPuAAsAjeQAcrXp4dK8tKX/AAyUDRskJEnBvK/c1sfo49Pl4NDTzr/HwrPJw7bg28vNx7nGwLLAuqy8tqnZ08TQyru+uKvVz8DBu624saTa1MXX0cLDvbG/uaq3sKNfRj3i3c7h3M3f2sve2Mnl4dEAgc/LxbbBvbDDuau7tKe8sKOknpacl5OnmYyLhoV7dndVUVoAidvRy7wAdrytqKGmn5dmYWdjXmS2CA7KjyJJAAAAWnRSTlMAUOn+7T029+vr6eXd1tDCooBXUTczMjAgFRURDQv7+vj28vDv7u7o3dnZ0sjFxMTExMLAs7OlpJWVkYyMiId+fnZ1bGtZVlZTT0xMRURERDMxLiYmIiIVEwd4pmW4AAABgUlEQVQoz3XQZVdjMRAG4JS2iy/O7sKyLri7u7szuUmu9kqNGu4Ov5opX+CclvdTzjxnJsmQN9nZXv1K0uXT0593oHdqKy2MjJK0WfjmXkxXX/u77/63nlqvrLned9/WVqbAbGN9Q1Nzy1xqi5SIBsD/pjA06eqf+IAg2YEDBG9xn6t4GKHuV87PrGlCgtLFhbxLSIkn8//vfIT2WNQMv3Q8PCahLBG3RQdCt8Rtp4IQym5kDeGLxOhBD8J8gJqtrs8uGg1wcYyHgnhQLCGU+HxaVsbHDE3WQiEHDznUdGYQxhSfqusWJGMA6NZ3zsU4woBPVR0Ay4LLyySFGFX1QYRO5erU0AG4fHYmcwDB7q5EF0KBcu93DANoTFIYBUNmoVPRhpAXxDtweEw6OtqjACajXK9GyFRsOXwMh/GTSOREOgTOTC5+4H+YYqphv6XtnUci5wnNCtqUi90KUsbwueH8okJPdm5utqewKA9HaX7c0Ua5t3Sl6nXbVeWl3uVN8gwRMmy+jlYyqwAAAABJRU5ErkJggg==);
}
.hpwuInn span {
  background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAABX1BMVEUAAAD/AADn6u98tB7/AA7/AA69yc3P19twgIeYaUp2eIB/uCKix2qryId4th97rR5djGJ6tR34AA2qh2qllGmff2x1eoOwlISFviuzxrThAQp5tR24xsCLvz/G2rOjxne0zJ5tgIm8z7N6tB+VY2NqgIrm6+//AA5OzuaebEnUy8V7th2Zc168s7C2nI2XaEmchm52nZeKhXGDvSXhAQrc4eb3AA3j6Ozf5urY3OHb3eDg2tzV09fWz8tdvMnK3bh7iHuigWWkfmGSel+SblTwR1H1LjqHvyn+CBbYAwjh5urf6OPY2d3R2Nzi1NrY5dXZ09DCxcvixMrQ4cPhvMPR4sDiq7Hjo6qpqaSsxKFxoqC71pu70JjkkJeFj5flh4+x0YimxYZ8j4Xle4SvyYKwmYLpbHSCemmawmXrWWOFcF3tUl2XdVe8qFaeclTxPUjzN0L5Hir7GSbkAQvWBAcsCSLkAAAAJnRSTlMAB2BEs7nAQED+8f7jcyEMBfvz8vLx7Ojm4NTSz7J1Z19UUEskGInQITEAAAEXSURBVCjPbczlUsNAFIbhbEuApIq7w1lSAilE6q64u7s79z/sph02KXlm9s/3zh7OAiHOEWrikfOOMSu2vVwmJdhKTNn2j2TyHfOT3aIojln30gpArIT5fk1btO7POhD6I26nge13UTBFr7Hx88328wjURU6xof3t+2Cxhw1U26s7YLNV5RHdv9agQeKTFs4FiRCxATqkQlQKXGSmYe4pm72XV8/03DyVY+EqnT6SN19j4WkqzEKxUHiRlw7gX1AkSZIBnMJbkYb8ApW3/VBIiM/WxBtOMfVwcmuGG4Dl7d3M+nHmYpjuwaHOHkUQhK4Or9ejqpWKqqq9ExznH3g4HGym+i5nTB7y2vxcwD3iG2+hRn1uJvAL1ThByGyKR5AAAAAASUVORK5CYII=);
}
.hpwuGreenhouse span {
  background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAABzlBMVEUAAADP4Un/4kv/3lT/1EL/xjL/31B2sh3/1EIyPx//1kSBvSWKxCf/10b/20t6tR3/0T//xjP/yzj/4FFzsSF0ryn/3Ez/zTtwqyT/00L/4FH/3k9mcTj/4VB6sSVlcTj/yjf/00F6tR3/4FH/2UmSyTJ5th3/2Uj/zzz/4VKRyDT/41P/41P/4VP/41X/40eAvxX/00P/1VWJwjCDvS6SyTKSyTF6th10riB2sCvmy0rcwkV6tR3/xTH/xTH/yjd2sSqSyTJ7tCr/yzd/uSySyTL/zTt6tR2yqUX/zTuXjzk9SiT/0kCSyTL/0kB5syP/20r/20v/10SPxjFweTtBSyP/zTr/3Uv/3E3/yDWAuSxznSyTyTJodDr/2kqTyjL/10f/zz57th15th16tB9ncjns3En/4FGUyTH/207/3kxibzs0QSF5tRtkdjeOxi+VyjWAsxp6tR3/wCpSNSn/yjdUOCv/zjt5UjlyTDX/xjJsSTFbPTFmRS//vCb/tSCLYTy7hDjsri+HwC5XOCv/2kr/1kWlcjazfTVhQjXkpzJvSzLxsi38vSxPMie1fzuZazhkRTdoRzWOxjHcnTH2tiv1tCvmoyv6tyhOMie25iI5AAAAcnRSTlMAAwQQ/rRM76icUC4J+fj27+zq5+fj4eDcyMKxramknJiShYR5dXJdVVRKSSUiGxIMCwb28/Lt7Orp5ubm4+Ld3dzc2dfKxsLBwcHBwMC7taikpKKgoJ2cmZSSin9lYFxaWldQS0NCOTkxLycnJhwbGBS33HA0AAABcklEQVQoz22QZXPCUBBFN0EL1N3di9bd3d3dveUlwd21bv+2hBkktOfbu2fu7NuFODAM/qEJBy4X8KY/Ym8JKyzE+LXMlFWPH5KbycllZB1ez4o3BTnlCJEkQuU5BYwK16lC7xbLF1I5ubEUX5/hZARceoLQuwIZnLkNPDpiDJnclN1gICi3CU3GhigE/f4XQqfR6IKv38MCRSRvzHSovHq7T632UXqvypHZGDHiqlyrJ9wgPJ+5VeLYLdaQyUwQBoOdeDahMiz6q4W03nSrmvoJUmprel/aIg5RLtmk/8lstnyQ7CuIpziFbzRqtUYjP6WYcatKSZ2Ww2ZztAJJJQsY1ObLu7rl+QeQyLkUJqZAera/U90gSnArq3A/+Gaz2ZqXhfT7oXq2rXW0AaA06bQkLy+rg8fLmheBbGtkaCA7u7PlAkqn7x7HlSHaa+gda1KVNLzt3aIkAOykouJYBmFuS0Kq5wiAFkxEQiHdvb6BGL+BS1sjzzwP5QAAAABJRU5ErkJggg==);
}

.HPWUButtons {
  color: #fff;
  padding: 3px;
}

.HPWUButtons span {
  float: none;
}

.notHPWU span {
    color: #FFF;
    background: #000;
    border-radius: 50%;
    font-size: 10px;
    letter-spacing: -0.15em;
    display: inline-block;
    opacity: 0.6;
    margin: 3px 1px 0 2px;
    height: 24px;
    width: 24px;
    box-sizing: border-box;
}

.notHPWU span:after {
    display: inline-block;
    content: "N/A";
    position: absolute;
    width: 24px;
    line-height: 24px;
    text-align: center;
    vertical-align: middle;
}

.notHPWU:focus span, .notHPWU.favorite span {
  opacity: 1;
}

.s2check-text {
  text-align: center;
  font-weight: bold;
  border: none !important;
  background: none !important;
  font-size: 130%;
  color: #000;
  text-shadow: 1px 1px #FFF, 2px 2px 6px #fff, -1px -1px #fff, -2px -2px 6px #fff;
}

#HPWUInnInfo {
  display: none;
    padding: 3px;
}

.isInn #HPWUInnInfo {
  display: block;
}

.thisIsHPWU .layer_off_warning,
.thisIsHPWU .mods,
.thisIsHPWU #randdetails,
.thisIsHPWU #resodetails,
.thisIsHPWU #level {
    display: none;
}

.thisIsHPWU #playerstat,
.thisIsHPWU #gamestat,
.thisIsHPWU #redeem,
.thisIsHPWU #chat,
.thisIsHPWU #artifactLink,
.thisIsHPWU #scoresLink,
.thisIsHPWU #chatinput,
.thisIsHPWU #chatcontrols {
    display: none;
}

.thisIsHPWU #mobileinfo .portallevel,
.thisIsHPWU #mobileinfo .resonator {
    display: none;
}

.thisIsHPWU #sidebar #portaldetails h3.title {
  color: #fff;
}

.inn {
    opacity: 0.8;
    stroke: #888;
}

.inn-wall{fill:#ddd;stroke-width:8;}
.inn-door{fill:#534C39;stroke-width:8;}
.inn-window{fill:#EBC360;stroke-width:4;}
.inn-roof{fill:#ddd;stroke-width:10;}
.inn-tower .inn-roof{stroke-width:5;}

.GreenColor {
  stroke: #4b474a;
}
.GreenColor .inn-wall {
  fill: #6a686f;
}
.GreenColor .inn-roof {
  fill: #59672e;
}

.PinkColor {
  stroke: #61646b;
}
.PinkColor .inn-wall {
  fill: #5b306f;
}
.PinkColor .inn-roof {
  fill: #1c6775;
}

.BlueColor {
  stroke: #45527c;
}
.BlueColor .inn-wall {
  fill: #868692;
}
.BlueColor .inn-roof {
  fill: #4162a9;
}

.BrownColor {
  stroke: #5b5c5e;
}
.BrownColor .inn-wall {
  fill: #6a7d7f;
}
.BrownColor .inn-roof {
  fill: #6b4a2e;
}

.PurpleColor {
  stroke: #47408e;
}
.PurpleColor .inn-wall {
  fill: #758093;
}
.PurpleColor .inn-roof {
  fill: #664b77;
}

.WhiteColor {
  stroke: #312c27;
}
.WhiteColor .inn-wall {
  fill: #b4b1a9;
}
.WhiteColor .inn-roof {
  fill: #2f4c58;
}

.smallinns .inn {
    opacity: 0.9;
}

.smallinns .inn svg {
  transform: scale(0.8);
}

.greenhouse {
  opacity: 0.8;
  stroke:#324E59;
  stroke-width:8;
}
.greenhouse-bottom{fill:#274042;}
.greenhouse-panes{fill:#339EC0;}
.greenhouse-frame{fill:none;stroke-width:3;}
.greenhouse-thick{stroke-width:6;}

.fortress {
  stroke:#373F4E;
}
.fortress-base{fill:#68655E;stroke-width:8;}
.fortress-steps{fill:#BDB9AD;stroke-width:2;}
.fortress-flag{fill:#CB5E35;stroke-width:2;}
.fortress-wall{fill:#8D7F65;stroke-width:6;}
.fortress-roof{fill:#D3867C;stroke-width:6;}
.fortress-window{fill:#FAE792;stroke-width:2;}
.fortress-thin{stroke-width:4;}
.fortress-door{fill:#62463A;stroke-width:2;}


.HPWUClassification div {
    display: grid;
    grid-template-columns: 200px 70px 90px 35px;
    text-align: center;
    align-items: center;
    height: 140px;
    overflow: hidden;
  margin-bottom: 10px;
}

.HPWUClassification div:nth-child(odd) {
  background: rgba(7, 42, 69, 0.9);
}

.HPWUClassification img {
    max-width: 200px;
  max-height: 140px;
    display: block;
    margin: 0 auto;
}

#dialog-missingPortals .HPWUClassification div {
  height: 50px;
}

img.photo,
.ingressLocation,
.hpwuLocation {
    cursor: zoom-in;
}

.HPWU-PortalAnimation {
  width: 30px;
  height: 30px;
  background-color: rgba(255, 255, 255, 0.5);
  border-radius: 50%;
  box-shadow: 0px 0px 4px white;
  animation-duration: 1s;
  animation-name: shrink;
}

@keyframes shrink {
  from {
    width: 30px;
    height: 30px;
    top: 0px;
    left: 0px;
  }

  to {
    width: 10px;
    height: 10px;
    top: 10px;
    left: 10px;
  }
}

.HPWU-PortalAnimationHover {
  background-color: rgb(255, 102, 0, 0.8);
  border-radius: 50%;
  animation-duration: 1s;
  animation-name: shrinkHover;
  animation-iteration-count: infinite;
}

@keyframes shrinkHover {
  from {
    width: 40px;
    height: 40px;
    top: 0px;
    left: 0px;
  }

  to {
    width: 20px;
    height: 20px;
    top: 10px;
    left: 10px;
  }
}

#sidebarHPWU {
    color: #eee;
    padding: 2px 5px;
}

#sidebarHPWU span {
    margin-right: 5px;
}

.refreshingData,
.refreshingPortalCount {
    opacity: 0.5;
  pointer-events: none;
}

#sidebarHPWU.mobile {
    width: 100%;
    background: rebeccapurple;
    display: flex;
}

#sidebarHPWU.mobile > div {
    margin-right: 1em;
}

.hpwu-colors input[type=color] {
  border: 0;
  padding: 0;
}

`).appendTo('head');
  };

  // A portal has been received.
  function onPortalAdded(data) {
    const guid = data.portal.options.guid;

    data.portal.on('add', function () {
      addNearbyCircle(guid);
    });

    data.portal.on('remove', function () {
      removeNearbyCircle(guid);
    });

    // analyze each portal only once, but sometimes the first time there's no additional data of the portal
    if (allPortals[guid] && allPortals[guid].name)
      return;

    const portal = {
      guid: guid,
      name: data.portal.options.data.title,
      lat: data.portal._latlng.lat,
      lng: data.portal._latlng.lng,
      image: data.portal.options.data.image,
      cells: {}
    };

    allPortals[guid] = portal;

    // If it's already classified in HPWU, get out
    const hpwuData = thisPlugin.findByGuid(guid);
    if (hpwuData) {
      const hpwuItem = hpwuData.store[guid];
      if (!hpwuItem.exists) {
        // Mark that it still exists in Ingress
        hpwuItem.exists = true;

        if (missingPortals[guid]) {
          delete missingPortals[guid];
          updateMissingPortalsCount();
        }

        // Check if it has been moved
        if (hpwuItem.lat != portal.lat || hpwuItem.lng != portal.lng) {
          movedPortals.push({
            hpwu: hpwuItem,
            ingress: portal
          });
          updateCounter('moved', movedPortals);
        }
      }
      if (!hpwuItem.name && portal.name) {
        hpwuData.store[guid].name = portal.name;
      }
      return;
    }

    if (skippedPortals[guid]/* || newPokestops[guid]*/)
      return;

    newPortals[guid] = portal;

    refreshNewPortalsCounter();
  }

  /**
   * Draw a 20m circle around a portal
   */
  function addNearbyCircle(guid) {
    const portal = window.portals[guid];
    if (!portal)
      return;

    const circleSettings = {
      color: settings.colors.nearbyCircleBorder.color,
      opacity: settings.colors.nearbyCircleBorder.opacity,
      fillColor: settings.colors.nearbyCircleFill.color,
      fillOpacity: settings.colors.nearbyCircleFill.opacity,
      weight: 1,
      clickable: false,
      interactive: false
    };

    const center = portal._latlng;
    const circle = L.circle(center, 20, circleSettings);
    nearbyGroupLayer.addLayer(circle);
    nearbyCircles[guid] = circle;
  }

  /**
   * Removes the 20m circle if a portal is purged
   */
  function removeNearbyCircle(guid) {
    const circle = nearbyCircles[guid];
    if (circle != null) {
      nearbyGroupLayer.removeLayer(circle);
      delete nearbyCircles[guid];
    }
  }

  function redrawNearbyCircles() {
    const keys = Object.keys(nearbyCircles);
    keys.forEach(guid => {
      removeNearbyCircle(guid);
      addNearbyCircle(guid);
    });
  }

  function refreshNewPortalsCounter() {
    if (!settings.analyzeForMissingData)
      return;

    // workaround for https://bugs.chromium.org/p/chromium/issues/detail?id=961199
    try
    {
      if (checkNewPortalsTimout) {
        clearTimeout(checkNewPortalsTimout);
      } else {
        document.getElementById('sidebarHPWU').classList.add('refreshingPortalCount');
      }
    } catch (e) {
      // nothing
    }

    // workaround for https://bugs.chromium.org/p/chromium/issues/detail?id=961199
    try
    {
      checkNewPortalsTimout = setTimeout(checkNewPortals, 1000);
    } catch (e) {
      checkNewPortals();
    }
  }

  /**
   * A potential new portal has been received
   */
  function checkNewPortals() {
    checkNewPortalsTimout = null;

    // don't try to classify if we don't have all the portal data
    if (map.getZoom() < 15)
      return;

    document.getElementById('sidebarHPWU').classList.remove('refreshingPortalCount');

    // newPokestops = {};
    notClassifiedPois = [];

    const allCells = groupByCell(17);

    // Check only the items inside the screen,
    // the server might provide info about remote portals if they are part of a link
    // and we don't know anything else about nearby portals of that one.
    // In this case (vs drawing) we want to filter only cells fully within the screen
    const cells = filterWithinScreen(allCells);

    // try to guess new pois if they are the only items in a cell
    Object.keys(cells).forEach(id => {
      const data = allCells[id];
      checkIsPortalMissing(data.inns, data);
      checkIsPortalMissing(data.fortresses, data);
      checkIsPortalMissing(data.greenhouses, data);
      //checkIsPortalMissing(data.nothpwu);

      if (data.notClassified.length == 0)
        return;
      const notClassified = data.notClassified;

      if (data.inns.length || data.fortresses.length || data.greenhouses.length) {
        // Already has a hpwu item, ignore the rest
        notClassified.forEach(portal => {
          skippedPortals[portal.guid] = true;
          delete newPortals[portal.guid];
        });
        return;
      }
      // // only one, let's guess it's a pokestop by default
      // if (notClassified.length == 1) {
      //   const portal = notClassified[0];
      //   const obj = {'guid': portal.guid, 'lat': portal.lat, 'lng': portal.lng, 'name': portal.name};
      //
      //   newPokestops[portal.guid] = obj;
      //   //delete newPortals[portal.guid];
      //   return;
      // }

      // too many items to guess
      notClassifiedPois.push(data.notClassified);
    });

    // updateCounter('pokestops', Object.values(newPokestops));
    updateCounter('classification', notClassifiedPois);
    updateMissingPortalsCount();

    // Now gyms
    // checkNewGyms();
  }

  /**
   * Filter the missing portals detection to show only those on screen and reduce false positives
   */
  function updateMissingPortalsCount() {
    const keys = Object.keys(missingPortals);
    if (keys.length == 0)
      updateCounter('missing', []);

    const bounds = map.getBounds();
    const filtered = [];
    keys.forEach(guid => {
      const hpwuData = thisPlugin.findByGuid(guid);
      const item = hpwuData.store[guid];
      if (isPointOnScreen(bounds, item)) {
        filtered.push(item);
      }
    });
    updateCounter('missing', filtered);
  }

  /**
   * Given an array of HPWU items checks if they have been removed from Ingress
   */
  function checkIsPortalMissing(array, cellData) {
    array.forEach(item => {
      if (item.exists || item.newGuid)
        return;
      const guid = item.guid;

      if (findCorrectGuid(item, cellData.notClassified)) {
        return;
      }
      if (!missingPortals[guid]) {
        missingPortals[guid] = true;
      }
    });
  }

  /**
   * Check if there's another real portal in the same cell (we're checking a poi that doesn't exist in Ingress)
   */
  function findCorrectGuid(hpwuItem, array) {
    const portal = array.find(x => x.name == hpwuItem.name && x.guid != hpwuItem.guid);
    if (portal != null) {
      hpwuItem.newGuid = portal.guid;
      movedPortals.push({
        hpwu: hpwuItem,
        ingress: portal
      });
      updateCounter('moved', movedPortals);

      delete missingPortals[hpwuItem.guid];

      return true;
    }
    return false;
  }

  // function checkNewGyms() {
  //   const cellsWithMissingGyms = [];
  //
  //   const allCells = groupByCell(14);
  //
  //   // Check only the items inside the screen,
  //   // the server might provide info about remote portals if they are part of a link
  //   // and we don't know anything else about nearby portals of that one.
  //   // In this case (vs drawing) we want to filter only cells fully within the screen
  //   const cells = filterWithinScreen(allCells);
  //
  //   // Find the cells where new Gyms can be identified
  //   Object.keys(cells).forEach(id => {
  //     const data = allCells[id];
  //     // Only cells with all the portals already analyzed
  //     if (data.notClassified.length > 0)
  //       return;
  //     if (ignoredCellsMissingGyms[data.cell.toString()])
  //       return;
  //     const missingGyms = computeMissingGyms(data);
  //     if (missingGyms > 0) {
  //       cellsWithMissingGyms.push(data);
  //     }
  //   });
  //
  //   if (cellsWithMissingGyms.length > 0) {
  //     const filtered = filterWithinScreen(cellsWithMissingGyms);
  //     updateCounter('gyms', Object.values(filtered));
  //   } else {
  //     updateCounter('gyms', []);
  //   }
  // }

  /**
   * Display new pokestops so they can be added
   */
  // function promptForNewPokestops(data) {
  //   if (data.length == 0)
  //     return;
  //   let pending = data.length;
  //
  //   const div = document.createElement('div');
  //   div.className = 'PogoClassification';
  //   data.sort(sortByName).forEach(portal => {
  //     const wrapper = document.createElement('div');
  //     wrapper.setAttribute('data-guid', portal.guid);
  //     const img = getPortalImage(portal);
  //     wrapper.innerHTML = '<span class="PogoName">' + getPortalName(portal) +
  //       img + '</span>' +
  //       '<a data-type="pokestops">' + 'STOP' + '</a>' +
  //       '<a data-type="gyms">' + 'GYM' + '</a>' +
  //       '<a data-type="notpogo">' + 'N/A' + '</a>';
  //     div.appendChild(wrapper);
  //   });
  //   const container = dialog({
  //     id: 'classifyPokestop',
  //     html: div,
  //     width: '420px',
  //     title: 'Are all of these Pokestops or Gyms?',
  //     buttons: {
  //       // Button to allow skip this cell
  //       'Skip': function () {
  //         container.dialog('close');
  //         data.forEach(portal => {
  //           delete newPokestops[portal.guid];
  //           skippedPortals[portal.guid] = true;
  //         });
  //         updateCounter('pokestops', Object.values(newPokestops));
  //       },
  //       'Mark all as Pokestops': function () {
  //         container.dialog('close');
  //         data.forEach(portal => {
  //           if (!newPokestops[portal.guid])
  //             return;
  //
  //           delete newPokestops[portal.guid];
  //           thisPlugin.addPortalpogo(portal.guid, portal.lat, portal.lng, portal.name, 'pokestops');
  //         });
  //         if (settings.highlightGymCandidateCells) {
  //           updateMapGrid();
  //         }
  //         updateCounter('pokestops', Object.values(newPokestops));
  //       }
  //     }
  //   });
  //   // Remove ok button
  //   const outer = container.parent();
  //   outer.find('.ui-dialog-buttonset button:first').remove();
  //
  //   // mark the selected one as pokestop or gym
  //   container.on('click', 'a', function (e) {
  //     const type = this.getAttribute('data-type');
  //     const row = this.parentNode;
  //     const guid = row.getAttribute('data-guid');
  //     const portal = allPortals[guid];
  //     delete newPokestops[portal.guid];
  //     thisPlugin.addPortalpogo(guid, portal.lat, portal.lng, portal.name, type);
  //     if (settings.highlightGymCandidateCells) {
  //       updateMapGrid();
  //     }
  //     $(row).fadeOut(200);
  //     pending--;
  //     if (pending == 0) {
  //       container.dialog('close');
  //     }
  //     updateCounter('pokestops', Object.values(newPokestops));
  //   });
  //
  //   container.on('click', 'img.photo', centerPortal);
  //   configureHoverMarker(container);
  // }

  /**
   * In a level 17 cell there's more than one portal, ask which one is Inn or Fortress or Greenhouse
   */
  function promptToClassifyPois() {
    updateCounter('classification', notClassifiedPois);
    if (notClassifiedPois.length == 0)
      return;

    const group = notClassifiedPois.shift();
    const div = document.createElement('div');
    div.className = 'HPWUClassification';
    group.sort(sortByName).forEach(portal => {
      const wrapper = document.createElement('div');
      wrapper.setAttribute('data-guid', portal.guid);
      const img = getPortalImage(portal);
      wrapper.innerHTML = '<span class="HPWUName">' + getPortalName(portal) +
        img + '</span>' +
        '<a data-type="fortressess">' + 'FORTRESS' + '</a> ' +
        '<a data-type="greenhouses">' + 'GREENHOUSE' + '</a> ' +
        '<a data-type="inns">' + 'INN' + '</a>';
      div.appendChild(wrapper);
    });
    const container = dialog({
      id: 'classifyPoi',
      html: div,
      width: '420px',
      title: 'Which one is in HPWU?',
      buttons: {
        // Button to allow skip this cell
        Skip: function () {
          container.dialog('close');
          group.forEach(portal => {
            delete newPortals[portal.guid];
            skippedPortals[portal.guid] = true;
          });
          // continue
          promptToClassifyPois();
        }
      }
    });
    // Remove ok button
    const outer = container.parent();
    outer.find('.ui-dialog-buttonset button:first').remove();

    // mark the selected one as inn or fortress or greenhouse
    container.on('click', 'a', function (e) {
      const type = this.getAttribute('data-type');
      const guid = this.parentNode.getAttribute('data-guid');
      const portal = getPortalSummaryFromGuid(guid);
      thisPlugin.addPortalHPWU(guid, portal.lat, portal.lng, portal.name, type);
      // if (settings.highlightGymCandidateCells) {
      //   updateMapGrid();
      // }

      group.forEach(tmpPortal => {
        delete newPortals[tmpPortal.guid];
      });

      container.dialog('close');
      // continue
      promptToClassifyPois();
    });
    container.on('click', 'img.photo', centerPortal);
    configureHoverMarker(container);
  }

  /**
   * List of portals that have been moved
   */
  function promptToMovePois() {
    if (movedPortals.length == 0)
      return;

    const div = document.createElement('div');
    div.className = 'HPWUClassification';
    movedPortals.sort(sortByName).forEach(pair => {
      const portal = pair.ingress;
      const hpwuItem = pair.hpwu;
      const wrapper = document.createElement('div');
      wrapper.setAttribute('data-guid', portal.guid);
      wrapper.dataPortal = portal;
      wrapper.dataHPWUGuid = hpwuItem.guid;
      const img = getPortalImage(portal);
      wrapper.innerHTML = '<span class="HPWUName">' + getPortalName(portal) +
        img + '</span>' +
        '<span><span class="ingressLocation">' + 'Ingress location' + '</span></span>' +
        '<span><span class="hpwuLocation" data-lat="' + hpwuItem.lat + '" data-lng="' + hpwuItem.lng + '">' + 'HPWU location' + '</span><br>' +
        '<a>' + 'Update' + '</a></span>';
      div.appendChild(wrapper);
    });
    const container = dialog({
      id: 'movedPortals',
      html: div,
      width: '420px',
      title: 'These portals have been moved in Ingress',
      buttons: {
        // Button to move all the portals at once
        'Update all': function () {
          container.dialog('close');
          movedPortals.forEach(pair => {
            const portal = pair.ingress;
            const hpwuItem = pair.hpwu;
            moveHPWU(portal, hpwuItem.guid);
          });
          movedPortals.length = 0;
          updateCounter('moved', movedPortals);

          thisPlugin.saveStorage();
          // if (settings.highlightGymCandidateCells) {
          //   updateMapGrid();
          // }

        }
      }
    });

    // Update location
    container.on('click', 'a', function (e) {
      const row = this.parentNode.parentNode;
      const portal = row.dataPortal;
      moveHPWU(portal, row.dataHPWUGuid);

      thisPlugin.saveStorage();
      if (settings.highlightHPWUCandidateCells) {
        updateMapGrid();
      }

      $(row).fadeOut(200);

      // remove it from the list of portals
      const idx = movedPortals.findIndex(pair => pair.ingress.guid == pair.ingress.guid);
      movedPortals.splice(idx, 1);
      updateCounter('moved', movedPortals);

      if (movedPortals.length == 0)
        container.dialog('close');
    });
    container.on('click', 'img.photo', centerPortal);
    container.on('click', '.ingressLocation', centerPortal);
    container.on('click', '.hpwuLocation', centerPortalAlt);
    configureHoverMarker(container);
    configureHoverMarkerAlt(container);
  }

  /**
   * Update location of a HPWU item
   */
  function moveHPWU(portal, hpwuGuid) {
    const guid = portal.guid;
    const hpwuData = thisPlugin.findByGuid(hpwuGuid);

    const existingType = hpwuData.type;
    // remove marker
    removeHPWUObject(existingType, guid);

    // Draw new marker
    thisPlugin.addPortalHPWU(guid, portal.lat, portal.lng, portal.name || hpwuData.name, existingType, hpwuData.color);
  }

  /**
   * HPWU items that aren't in Ingress
   */
  function promptToRemovePois(missing) {
    const div = document.createElement('div');
    div.className = 'HPWUClassification';
    missing.sort(sortByName).forEach(portal => {
      const wrapper = document.createElement('div');
      wrapper.setAttribute('data-guid', portal.guid);
      const name = portal.name || 'Unknown';
      wrapper.innerHTML = '<span class="HPWUName"><span class="hpwuLocation" data-lat="' + portal.lat + '" data-lng="' + portal.lng + '">' + name + '</span></span>' +
        '<span><a>' + 'Remove' + '</a></span>';
      div.appendChild(wrapper);
    });
    const container = dialog({
      id: 'missingPortals',
      html: div,
      width: '420px',
      title: 'These portals are missing in Ingress',
      buttons: {
      }
    });

    // Update location
    container.on('click', 'a', function (e) {
      const row = this.parentNode.parentNode;
      const guid = row.getAttribute('data-guid');
      const hpwuData = thisPlugin.findByGuid(guid);
      const existingType = hpwuData.type;

      // remove marker
      removeHPWUObject(existingType, guid);
      thisPlugin.saveStorage();

      // if (settings.highlightGymCandidateCells) {
      //   updateMapGrid();
      // }

      $(row).fadeOut(200);

      delete missingPortals[guid];
      updateMissingPortalsCount();

      if (Object.keys(missingPortals).length == 0) {
        container.dialog('close');
      }
    });
    container.on('click', '.hpwuLocation', centerPortalAlt);
    configureHoverMarkerAlt(container);
  }

  function configureHoverMarker(container) {
    let hoverMarker;
    container.find('img.photo, .ingressLocation').hover(
      function hIn() {
        const row = this.parentNode.parentNode;
        const guid = row.getAttribute('data-guid');
        const portal = row.dataPortal || window.portals[guid];
        if (!portal)
          return;
        const center = portal._latlng || new L.LatLng(portal.lat, portal.lng);
        hoverMarker = L.marker(center, {
          icon: L.divIcon({
            className: 'HPWU-PortalAnimationHover',
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            html: ''
          }),
          interactive: false
        });
        map.addLayer(hoverMarker);
      }, function hOut() {
        if (hoverMarker)
          map.removeLayer(hoverMarker);
      });
  }

  function configureHoverMarkerAlt(container) {
    let hoverMarker;
    container.find('.hpwuLocation').hover(
      function hIn() {
        const lat = this.getAttribute('data-lat');
        const lng = this.getAttribute('data-lng');
        const center = new L.LatLng(lat, lng);
        hoverMarker = L.marker(center, {
          icon: L.divIcon({
            className: 'HPWU-PortalAnimationHover',
            iconSize: [40, 40],
            iconAnchor: [20, 20],
            html: ''
          }),
          interactive: false
        });
        map.addLayer(hoverMarker);
      }, function hOut() {
        if (hoverMarker)
          map.removeLayer(hoverMarker);
      });
  }

  /**
   * Center the map on the clicked portal to help tracking it (the user will have to manually move the dialog)
   */
  function centerPortal(e) {
    const row = this.parentNode.parentNode;
    const guid = row.getAttribute('data-guid');
    const portal = row.dataPortal || window.portals[guid];
    if (!portal)
      return;
    const center = portal._latlng || new L.LatLng(portal.lat, portal.lng);
    map.panTo(center);
    drawClickAnimation(center);
  }

  function centerPortalAlt(e) {
    const lat = this.getAttribute('data-lat');
    const lng = this.getAttribute('data-lng');
    const center = new L.LatLng(lat, lng);
    map.panTo(center);
    drawClickAnimation(center);
  }

  function drawClickAnimation(center) {
    const marker = L.marker(center, {
      icon: L.divIcon({
        className: 'HPWU-PortalAnimation',
        iconSize: [30, 30],
        iconAnchor: [15, 15],
        html: ''
      }),
      interactive: false
    });
    map.addLayer(marker);

    setTimeout(function () {
      map.removeLayer(marker);
    }, 2000);
  }

  function getPortalSummaryFromGuid(guid) {
    const newPortal = newPortals[guid];
    if (newPortal)
      return newPortal;

    const portal = window.portals[guid];
    if (!portal)
      return {};

    return {
      guid: guid,
      name: portal.options.data.title,
      lat: portal._latlng.lat,
      lng: portal._latlng.lng,
      image: portal.options.data.image,
      cells: {}
    };
  }

  function getPortalImage(poi) {
    if (poi.image)
      return '<img src="' + poi.image.replace('http:', 'https:') + '" class="photo">';

    const portal = window.portals[poi.guid];
    if (!portal)
      return '';

    if (portal && portal.options && portal.options.data && portal.options.data.image) {
      poi.image = portal.options.data.image;
      return '<img src="' + poi.image.replace('http:', 'https:') + '" class="photo">';
    }
    return '';
  }

  function getPortalName(poi) {
    if (poi.name)
      return poi.name;

    const portal = window.portals[poi.guid];
    if (!portal)
      return '';

    if (portal && portal.options && portal.options.data && portal.options.data.title) {
      poi.name = portal.options.data.title;
      return poi.name;
    }
    return '';
  }

  /**
   * In a level 14 cell there's some missing Gyms, prompt which ones
   */
  // function promptToClassifyGyms(groups) {
  //   // don't try to classify if we don't have all the portal data
  //   if (map.getZoom() < 15)
  //     return;
  //
  //   if (!groups || groups.length == 0)
  //     return;
  //
  //   const cellData = groups.shift();
  //   updateCounter('gyms', groups);
  //
  //   let missingGyms = computeMissingGyms(cellData);
  //
  //   const div = document.createElement('div');
  //   div.className = 'PogoClassification';
  //   cellData.stops.sort(sortByName).forEach(portal => {
  //     if (skippedPortals[portal.guid])
  //       return;
  //
  //     const wrapper = document.createElement('div');
  //     wrapper.setAttribute('data-guid', portal.guid);
  //     wrapper.innerHTML =
  //       '<span class="PogoName">' + getPortalName(portal) +
  //       getPortalImage(portal) + '</span>' +
  //       '<a data-type="gyms">' + 'GYM' + '</a>';
  //     div.appendChild(wrapper);
  //   });
  //   // No pokestops to prompt as it has been skipped
  //   if (!div.firstChild) {
  //     // continue
  //     promptToClassifyGyms(groups);
  //     return;
  //   }
  //
  //   const container = dialog({
  //     id: 'classifyPokestop',
  //     html: div,
  //     width: '360px',
  //     title: missingGyms == 1 ? 'Which one is a Gym?' : 'Which ' + missingGyms + ' are Gyms?',
  //     buttons: {
  //       // Button to allow skip this cell
  //       Skip: function () {
  //         container.dialog('close');
  //         cellData.stops.forEach(portal => {
  //           skippedPortals[portal.guid] = true;
  //         });
  //         // continue
  //         promptToClassifyGyms(groups);
  //       },
  //       // Button to allow skip this cell
  //       'There is no Gym': function () {
  //         ignoredCellsMissingGyms[cellData.cell.toString()] = true;
  //
  //         if (settings.highlightGymCandidateCells) {
  //           updateMapGrid();
  //         }
  //         container.dialog('close');
  //
  //         thisPlugin.saveStorage();
  //
  //         updateCounter('gyms', groups);
  //         // continue
  //         promptToClassifyGyms(groups);
  //       }
  //     }
  //   });
  //   // Remove ok button
  //   const outer = container.parent();
  //   outer.find('.ui-dialog-buttonset button:first').remove();
  //
  //   // mark the selected one as pokestop or gym
  //   container.on('click', 'a', function (e) {
  //     const type = this.getAttribute('data-type');
  //     const row = this.parentNode;
  //     const guid = row.getAttribute('data-guid');
  //     const portal = pokestops[guid];
  //
  //     removePogoObject('pokestops', guid);
  //
  //     thisPlugin.addPortalpogo(guid, portal.lat, portal.lng, portal.name, type);
  //     if (settings.highlightGymCandidateCells) {
  //       updateMapGrid();
  //     }
  //     missingGyms--;
  //     if (missingGyms == 0) {
  //       container.dialog('close');
  //       // continue
  //       promptToClassifyGyms(groups);
  //     } else {
  //       $(row).fadeOut(200);
  //       document.querySelector('.ui-dialog-title-active').textContent = missingGyms == 1 ? 'Which one is a Gym?' : 'Which ' + missingGyms + ' are Gyms?';
  //     }
  //   });
  //
  //   container.on('click', 'img.photo', centerPortal);
  //   configureHoverMarker(container);
  // }

  /**
   * In a level 14 cell there are too many Gyms
   */
  // function promptToVerifyGyms(cellIds) {
  //   if (!cellIds)
  //     cellIds = Object.keys(cellsExtraGyms);
  //
  //   if (cellIds.length == 0)
  //     return;
  //
  //   const cellId = cellIds[0];
  //   const group = findCellItems(cellId, 14, gyms);
  //
  //   const div = document.createElement('div');
  //   div.className = 'PogoClassification';
  //   group.sort(sortByName).forEach(portal => {
  //     const wrapper = document.createElement('div');
  //     wrapper.setAttribute('data-guid', portal.guid);
  //     const img = getPortalImage(portal);
  //     wrapper.innerHTML = '<span class="PogoName">' + getPortalName(portal) +
  //       img + '</span>' +
  //       '<a data-type="pokestops">' + 'STOP' + '</a>';
  //     div.appendChild(wrapper);
  //   });
  //   const container = dialog({
  //     id: 'classifyPokestop',
  //     html: div,
  //     width: '360px',
  //     title: 'This cell has too many Gyms.',
  //     buttons: {
  //       // Button to allow skip this cell
  //       'All are OK': function () {
  //         ignoredCellsExtraGyms[cellId] = true;
  //
  //         if (settings.highlightGymCandidateCells) {
  //           updateMapGrid();
  //         }
  //         container.dialog('close');
  //         delete cellsExtraGyms[cellId];
  //
  //         thisPlugin.saveStorage();
  //
  //         updateCounter('extraGyms', Object.keys(cellsExtraGyms));
  //         // continue
  //         promptToVerifyGyms();
  //       }
  //     }
  //   });
  //   // Remove ok button
  //   const outer = container.parent();
  //   outer.find('.ui-dialog-buttonset button:first').remove();
  //
  //   // mark the selected one as pokestop or gym
  //   container.on('click', 'a', function (e) {
  //     const type = this.getAttribute('data-type');
  //     const guid = this.parentNode.getAttribute('data-guid');
  //     const portal = gyms[guid];
  //     thisPlugin.addPortalpogo(guid, portal.lat, portal.lng, portal.name, type);
  //     if (settings.highlightGymCandidateCells) {
  //       updateMapGrid();
  //     }
  //
  //     container.dialog('close');
  //     delete cellsExtraGyms[cellId];
  //     updateCounter('extraGyms', Object.keys(cellsExtraGyms));
  //     // continue
  //     promptToVerifyGyms();
  //   });
  //   container.on('click', 'img.photo', centerPortal);
  //   configureHoverMarker(container);
  // }


  function removeLayer(name) {
    const layers = window.layerChooser._layers;
    const layersIds = Object.keys(layers);

    let layerId = null;
    let leafletLayer;
    let isBase;
    let arrayIdx;
    layersIds.forEach(id => {
      const layer = layers[id];
      if (layer.name == name) {
        leafletLayer = layer.layer;
        layerId = leafletLayer._leaflet_id;
        isBase = !layer.overlay;
        arrayIdx = id;
      }
    });

    // The Beacons and Frackers are not there in Firefox, why????
    if (!leafletLayer) {
      return;
    }

    const enabled = map._layers[layerId] != null;
    if (enabled) {
      // Don't remove base layer if it's used
      if (isBase)
        return;

      map.removeLayer(leafletLayer);
    }
    if (typeof leafletLayer.off != 'undefined')
      leafletLayer.off();

    // new Leaflet
    if (Array.isArray(layers)) {
      // remove from array
      layers.splice(parseInt(arrayIdx, 10), 1);
    } else {
      // classic IITC, leaflet 0.7.7
      // delete from object
      delete layers[layerId];
    }
    window.layerChooser._update();
    removedLayers[name] = {
      layer: leafletLayer,
      enabled: enabled,
      isBase: isBase
    };
    window.updateDisplayedLayerGroup(name, enabled);
  }
  const removedLayers = {};
  let portalsLayerGroup;

  function removeIngressLayers() {
    removeLayer('CartoDB Dark Matter');
    removeLayer('CartoDB Positron');
    removeLayer('Google Default Ingress Map');

    removeLayer('Fields');
    removeLayer('Links');
    removeLayer('DEBUG Data Tiles');
    removeLayer('Artifacts');
    removeLayer('Ornaments');
    removeLayer('Beacons');
    removeLayer('Frackers');

    removeLayer('Unclaimed/Placeholder Portals');
    for (let i = 1; i <= 8; i++) {
      removeLayer('Level ' + i + ' Portals');
    }
    //removeLayer('Resistance');
    //removeLayer('Enlightened');
    mergePortalLayers();
  }

  /**
   * Put all the layers for Ingress portals under a single one
   */
  function mergePortalLayers() {
    portalsLayerGroup = new L.LayerGroup();
    window.addLayerGroup('Ingress Portals', portalsLayerGroup, true);
    portalsLayerGroup.addLayer(removedLayers['Unclaimed/Placeholder Portals'].layer);
    for (let i = 1; i <= 8; i++) {
      portalsLayerGroup.addLayer(removedLayers['Level ' + i + ' Portals'].layer);
    }
    //portalsLayerGroup.addLayer(removedLayers['Resistance'].layer);
    //portalsLayerGroup.addLayer(removedLayers['Enlightened'].layer);
  }

  /**
   * Remove the single layer for all the portals
   */
  function revertPortalLayers() {
    if (!portalsLayerGroup) {
      return;
    }
    const name = 'Ingress Portals';
    const layerId = portalsLayerGroup._leaflet_id;
    const enabled = map._layers[layerId] != null;

    const layers = window.layerChooser._layers;
    if (Array.isArray(layers)) {
      // remove from array
      const idx = layers.findIndex(o => o.layer._leaflet_id == layerId);
      layers.splice(idx, 1);
    } else {
      // classic IITC, leaflet 0.7.7
      // delete from object
      delete layers[layerId];
    }
    window.layerChooser._update();
    window.updateDisplayedLayerGroup(name, enabled);

    if (typeof portalsLayerGroup.off != 'undefined')
      portalsLayerGroup.off();
    if (enabled) {
      map.removeLayer(portalsLayerGroup);
    }
    portalsLayerGroup = null;
  }

  function restoreIngressLayers() {
    revertPortalLayers();

    Object.keys(removedLayers).forEach(name => {
      const info = removedLayers[name];
      if (info.isBase)
        window.layerChooser.addBaseLayer(info.layer, name);
      else
        window.addLayerGroup(name, info.layer, info.enabled);
    });
  }

  function zoomListener() {
    const zoom = map.getZoom();
    document.body.classList.toggle('smallinns', zoom < 16);
  }

  const setup = function () {
    thisPlugin.isSmart = window.isSmartphone();

    initSvgIcon();

    loadSettings();

    // Load data from localStorage
    thisPlugin.loadStorage();
    thisPlugin.syncLS();

    thisPlugin.htmlStar = `<a class="hpwuFortress" accesskey="f" onclick="window.plugin.hpwu.switchStarPortal('fortresses');return false;" title="Mark this portal as a fortress [f]"><span></span></a>
      <a class="hpwuGreenhouse" accesskey="g" onclick="window.plugin.hpwu.switchStarPortal('greenhouses');return false;" title="Mark this portal as a Greenhouse [g]"><span></span></a>
      <a class="hpwuInn" accesskey="i" onclick="window.plugin.hpwu.switchStarPortal('inns');return false;" title="Mark this portal as an Inn [i]"><span></span></a>
      <a class="notHPWU" onclick="window.plugin.hpwu.switchStarPortal('nothpwu');return false;" title="Mark this portal as a removed/Not Available in HPWU"><span></span></a>
      `;

    thisPlugin.setupCSS();

    const sidebarHPWU = document.createElement('div');
    sidebarHPWU.id = 'sidebarHPWU';
    sidebarHPWU.style.display = 'none';
    if (thisPlugin.isSmart) {
      const status = document.getElementById('updatestatus');
      sidebarHPWU.classList.add('mobile');
      status.insertBefore(sidebarHPWU, status.firstElementChild);

      const dStatus = document.createElement('div');
      dStatus.className = 'HPWUStatus';
      status.insertBefore(dStatus, status.firstElementChild);
    } else {
      document.getElementById('sidebar').appendChild(sidebarHPWU);
    }

    // sidebarPogo.appendChild(createCounter('New pokestops', 'pokestops', promptForNewPokestops));
    sidebarHPWU.appendChild(createCounter('Review required', 'classification', promptToClassifyPois));
    sidebarHPWU.appendChild(createCounter('Moved portals', 'moved', promptToMovePois));
    sidebarHPWU.appendChild(createCounter('Missing portals', 'missing', promptToRemovePois));
    // sidebarPogo.appendChild(createCounter('New Gyms', 'gyms', promptToClassifyGyms));
    // sidebarPogo.appendChild(createCounter('Cells with extra Gyms', 'extraGyms', promptToVerifyGyms));

    window.addHook('portalSelected', thisPlugin.onPortalSelected);

    window.addHook('portalAdded', onPortalAdded);
    window.addHook('mapDataRefreshStart', function () {
      sidebarHPWU.classList.add('refreshingData');
    });
    window.addHook('mapDataRefreshEnd', function () {
      sidebarHPWU.classList.remove('refreshingData');
      refreshNewPortalsCounter();
    });
    map.on('moveend', function () {
      refreshNewPortalsCounter();
    });
    sidebarHPWU.classList.add('refreshingData');

    // Layer - HPWU portals
    fortressLayerGroup = new L.LayerGroup();
    window.addLayerGroup('Fortresses', fortressLayerGroup, true);
    innLayerGroup = new L.LayerGroup();
    window.addLayerGroup('Inns', innLayerGroup, true);
    greenhouseLayerGroup = new L.LayerGroup();
    window.addLayerGroup('Greenhouses', greenhouseLayerGroup, true);
    regionLayer = L.layerGroup();
    window.addLayerGroup('S2 Grid', regionLayer, true);

    // this layer will group all the nearby circles that are added or removed from it when the portals are added or removed
    nearbyGroupLayer = L.layerGroup();

    thisPlugin.addAllMarkers();

    const toolbox = document.getElementById('toolbox');

    const buttonHPWU = document.createElement('a');
    buttonHPWU.textContent = 'HPWU Actions';
    buttonHPWU.title = 'Actions on HPWU data';
    buttonHPWU.addEventListener('click', thisPlugin.hpwuActionsDialog);
    toolbox.appendChild(buttonHPWU);

    const buttonGrid = document.createElement('a');
    buttonGrid.textContent = 'HPWU Settings';
    buttonGrid.title = 'Settings for S2 & HPWU';
    buttonGrid.addEventListener('click', e => {
      if (thisPlugin.isSmart)
        window.show('map');
      showS2Dialog();
    });
    toolbox.appendChild(buttonGrid);

    map.on('zoomend', zoomListener);
    zoomListener();
    map.on('moveend', updateMapGrid);
    updateMapGrid();

    // add ids to the links that we want to be able to hide
    const links = document.querySelectorAll('#toolbox > a');
    links.forEach(a => {
      const text = a.textContent;
      if (text == 'Region scores') {
        a.id = 'scoresLink';
      }
      if (text == 'Artifacts') {
        a.id = 'artifactLink';
      }
    });

  };

  function createCounter(title, type, callback) {
    const div = document.createElement('div');
    div.style.display = 'none';
    const sTitle = document.createElement('span');
    sTitle.textContent = title;
    const counter = document.createElement('a');
    counter.id = 'HPWUCounter-' + type;
    counter.addEventListener('click', function (e) {
      callback(counter.HPWUData);
      return false;
    });
    div.appendChild(sTitle);
    div.appendChild(counter);
    return div;
  }

  function updateCounter(type, data) {
    const counter = document.querySelector('#HPWUCounter-' + type);
    counter.HPWUData = data;
    counter.textContent = data.length;
    counter.parentNode.style.display = data.length > 0 ? '' : 'none';

    // Adjust visibility of the pane to avoid the small gap due to padding
    const pane = counter.parentNode.parentNode;
    if (data.length > 0) {
      pane.style.display = '';
      return;
    }
    let node = pane.firstElementChild;
    while (node) {
      const rowData = node.lastElementChild.HPWUData;
      if (rowData && rowData.length > 0) {
        pane.style.display = '';
        return;
      }
      node = node.nextElementSibling;
    }
    pane.style.display = 'none';
  }

  // PLUGIN END //////////////////////////////////////////////////////////

  setup.info = plugin_info; //add the script info data to the function as a property
  // if IITC has already booted, immediately run the 'setup' function
  if (window.iitcLoaded) {
    setup();
  } else {
    if (!window.bootPlugins) {
      window.bootPlugins = [];
    }
    window.bootPlugins.push(setup);
  }
}

const plugin_info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) {
  plugin_info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
  };
}

// Greasemonkey. It will be quite hard to debug
if (typeof unsafeWindow != 'undefined' || typeof GM_info == 'undefined' || GM_info.scriptHandler != 'Tampermonkey') {
  // inject code into site context
  const script = document.createElement('script');
  script.appendChild(document.createTextNode('(' + wrapperS2 + ')();'));
  script.appendChild(document.createTextNode('(' + wrapperPlugin + ')(' + JSON.stringify(plugin_info) + ');'));
  (document.body || document.head || document.documentElement).appendChild(script);
} else {
  // Tampermonkey, run code directly
  wrapperS2();
  wrapperPlugin(plugin_info);
}
